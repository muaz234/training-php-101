<div class="custom-flex-modal-header custom-modal-header">
    <div class="custom-flex-modal-header-logo display-hide-mobile">
        <img class="custom-modal-header-icon" src="assets/global/img/logo/modal-header-logo.png"/>
    </div>
    <div class="custom-flex-modal-header-title">
        <h4 class="custom-modal-title"><span class="bold uppercase"><i></i>&nbsp;&nbsp;<span class="custom-modal-title-text"></span></span></h4>
    </div>
    <div class="custom-flex-modal-header-close">
        <a class="btn custom-modal-balance-button tooltips" data-placement="bottom" data-toggle="tooltip" data-original-title="SEIMBANGKAN"><i class="fa fa-clone"></i></a>
        <a class="btn custom-modal-expand-button tooltips" data-placement="bottom" data-toggle="tooltip" data-original-title="BESAR/KECIL"><i class="fa fa-expand"></i></a>
        <button type="button" class="btn custom-modal-close-button tooltips" data-placement="bottom" data-toggle="tooltip" data-original-title="TUTUP" data-dismiss="modal" aria-hidden="true">X</button>
    </div>
</div>