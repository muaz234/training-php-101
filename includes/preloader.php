<?php
$preloader_display = "";
if(isset($data_noredirect['flag_value']) && $data_noredirect['flag_value'] == "1"){
    $preloader_display = "display:none";
}
?>
<div class='preloader-overlay-firstload' style="<?php echo $preloader_display;?>">
    <div class="preloader-content-firstload">
        <img src="<?php // echo $data_layout['path_loadingicon']; ?>">
        <h4 class="preloader-username"><?php // echo $_SESSION['user_fullname']; ?></h4>
        <strong><?php // echo $lang['reload_header']; ?></strong> <i class="fa fa-wifi faa-flash animated"></i><br />
        <span><?php // echo $lang['reload_text']; ?></span>
    </div>
</div>

<div class='preloader-overlay'>
    <div class="preloader-content">
        <img src="<?php // echo $data_layout['path_loadingicon']; ?>">
        <p style="margin:5px">
            <strong><?php //echo $lang['reload_header']; ?></strong> <i class="fa fa-wifi faa-flash animated"></i><br />
            <span id="preloader_reload_text"><?php // echo $lang['reload_text']; ?></span>
        </p>
    </div>
</div>

<div class='preloader-overlay-nonetwork'>
    <div class="preloader-error preloader-content-nonetwork">
        <img src="<? // echo $data_layout['path_loadingicon']; ?>">
        <p style="margin:5px">
            <strong><?php //echo $lang['reload_header_nonetwork']; ?></strong> <i class="fa fa-wifi faa-flash animated"></i><br />
            <span><?php //echo $lang['reload_text_nonetwork']; ?></span>
        </p>
    </div>
</div>

<div class='preloader-overlay-logoutwarn'>
    <div class="preloader-error preloader-content-logoutwarn">
        <img src="<?php //echo $data_layout['path_loadingicon']; ?>">
        <p style="margin:5px">
            <strong><?php // echo $lang['logout_header_warn']; ?></strong> <i class="fa fa-power-off faa-flash animated"></i><br />
            <span><?php // echo $lang['logout_text_warn']; ?></span><br>
            <span class="custom-logoutwarn-timer"><span id="custom_logoutwarn_timer"></span> <?php //echo $lang['second']; ?></span>
        </p>
    </div>
</div>