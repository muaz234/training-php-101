<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner display-hide-mobile"> <img class="custom-footer-icon" src="<?php //echo $data_layout['path_footericon']; ?>" /> <span><?php // echo $lang['footer']; ?></span>
    </div>
    <div class="page-footer-inner display-show-mobile display-hide-desktop display-hide-tablet"> <img class="custom-footer-icon" src="<?php // echo $data_layout['path_footericon']; ?>" /> <span><?php // echo $lang['footer_resp']; ?></span>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
    <div class="custom-footer-privacy-policy pull-right">
        <a class="custom-event-click"
           data-param='{
                            "target":"privacy_policy",
                            "click_id":"#privacy_policy_list_link",
                            "modal_id":"#privacy_policy_modal",
                            "title_icon":"fa fa-user",
                            "title_text":"<?php // echo $lang['privacy_policy']." ".$system_settings['customer_short'];?>"
                        }'
        >
            <?php // echo $lang['privacy_policy']; ?></a>
    </div>
</div>
<!-- END FOOTER -->
