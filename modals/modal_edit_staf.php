<?php ?>
<div id="kemaskini_pekerja_modal" class="modal container fade" tabindex="-1" data-width="70%">
    <div class="modal-body custom-modal-body">
        <div class="portlet box custom-modal-portlet-box">
            <?php include("includes/modal_header.php"); ?>
            <div class="portlet-body custom-modal-portlet-body">
                <div class="custom-modal-div-body">
                    <form name="edit_staf_form" id="edit_staf_form" method="POST" action="">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-2 ">
                                    <label for="firstname" class="required">Nama Pertama </label>
                                    <span class="required" style="color: red;">*</span>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="nama_pertama" id="nama_pertama" required/>
                                </div>
                                <div class="col-md-2">
                                    <label for="lastname" class="required">Nama Akhir </label>
                                    <span class="required" style="color: red;">*</span>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="nama_akhir" id="nama_akhir" required/>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-2">
                                    <label for="jantina" class="required">Jantina </label>
                                    <span class="required" style="color: red;">*</span>
                                </div>

                                <div class="col-md-4">
                                    <select class="form-control select2" name="jantina" id="jantina" required>
                                        <option value="">Sila Pilih</option>
                                        <option value="M">Lelaki</option>
                                        <option value="F">Perempuan</option>
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="objektif" class="required">Tarikh Mula Bekerja </label>
                                    <span class="required" style="color: red;">*</span>
                                </div>


                                <div class="col-md-4">
                                    <input type="text" name="tarikh_mula" id="tarikh_mula" placeholder="Tarikh Mula Bekerja" class="form-control" readonly />
                                </div>

                            </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <button type="button" name="staf_submit_btn" id="staf_submit_btn"
                                            class="btn btn-primary"> Kemaskini
                                    </button>
                                    <button type="button" name="staf_kembali_btn" id="staf_kembali_btn" onclick=""
                                            class="btn btn-default" data-dismiss="modal" aria-label="Kembali"> Tutup
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
        <?php include("includes/modal_footer.php"); ?>
    </div>
</div>