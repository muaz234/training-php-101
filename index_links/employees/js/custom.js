$(document).ready(function () {
    $('.m-subheader__title display-hide-mobile').text('Tetapan Latihan');
    $('.m-subheader__title display-show-mobile display-hide-desktop display-hide-tablet').text('Tetapan Latihan');

    var senarai_pekerja = $('#tbl_senarai_pekerja');
    senarai_pekerja.DataTable({
        "processing": true,
        ajax: {
            url: "backend/ws_employees.php?method=senarai_pekerja"
        },
        language: {
            processing: "Sedang memproses data...",
            search: "Carian&nbsp;:",
            info: "Paparan dari _START_ hingga _END_ dari  _TOTAL_ rekod ",
            loadingRecords: "Sedang memuat naik data...",
            zeroRecords: "Tiada data maklumat",
            emptyTable: "Tiada data maklumat",
            paginate: {
                previous: "Sebelum",
                next: "Kemudian"
            }
        },
        columns: [
            { data: "id_pekerja" },
            { data: "nama_pekerja" },
            { data: "jantina", render: function(data, type, row, meta)
                    {
                        return (data == 'M') ? 'Lelaki': 'Perempuan'
                    }
                },
            { data: "jawatan" },
            { data: "tarikh_mula_bekerja" },
            {data: null, render: function(data, type, row, meta) {
                    return '<button type="button" class="btn btn-xs btn-primary btn-papar-pekerja" id="'+row.id_pekerja+'"><i class="fa fa-edit"></i>&nbsp; Kemaskini</button>'
                } }
        ]
    });

    senarai_pekerja.on('click', '.btn-papar-pekerja', function(){
       var id_pekerja = this.id;

       $('.custom-modal-title-text').html("BUTIRAN PEKERJA");
       $('#jantina').select2({
           placeholder: "Pilih Jantina",
           width: '100%'
       });
       $('#kemaskini_pekerja_modal').modal('show');
       var data = [];
       data.push({name: 'method', value: 'get_details_employee'});
       data.push({name: 'id_pekerja', value: id_pekerja});

       $.get("backend/ws_employees.php", data).done(function(data){
           var JSONdata = JSON.parse(data);
            // console.log(JSONdata);
           var id_pekerja = JSONdata.emp_no;
           var nama_pertama = JSONdata.first_name;
           var nama_akhir = JSONdata.last_name;
           var jantina = JSONdata.gender;
           var tarikh_mula = JSONdata.hire_date;
            $('#nama_pertama').val(nama_pertama);
            $('#nama_akhir').val(nama_akhir);
            $('#jantina').val(jantina).trigger('change');
            $('#tarikh_mula').val(tarikh_mula);

            $('#staf_submit_btn').on('click', function (e) {
                e.preventDefault();

                $.ajax({
                    type: "POST",
                    url: "backend/ws_employees.php",
                    data: {
                        method: 'update_employee',
                        emp_no: id_pekerja,
                        first_name: $('#nama_pertama').val(),
                        last_name: $('#nama_akhir').val(),
                        gender: $('#jantina').val(),
                    },
                    dataType: "json",
                    success: function (response) {
                        console.log(response);
                        $('#kemaskini_pekerja_modal').modal('hide');
                        if(response.statusCode == 200)
                        {
                            swal('Berjaya!', 'Data Staf Berjaya Dikemaskini', 'success').then( () => location.reload() );
                        }
                        else
                        {
                            swal('Gagal!', 'Data Staf Gagal Dikemaskini', 'error').then( () => location.reload() );
                        }
                    },
                    error: function (jxHqr, responseText, errorThrown) {
                        console.log(jxHqr);
                        console.log(responseText);
                        console.log(errorThrown);
                    }
                })

            })
       });

    });



})