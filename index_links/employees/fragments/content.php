<?php ?>
<div class="container-fluid">
    <br>
    <div class="portlet light portlet-fit portlet-datatable bordered">

        <div class="portlet-title">
            <div class="caption">
                <i class="icon-info"></i>
                <span class="caption-subject bold uppercase">Info Pekerja</span>
            </div>
            <div class="actions">
                <span id="btn_tambah_daerah" class="btn btn-outline blue" ><i class="fa fa-plus" ></i>&nbsp; Tambah Pekerja</span>
            </div>
        </div>

        <div class="portlet-body">
            <table class="table table-striped table-bordered table-hover table-full-width" style="width: 100%;" id="tbl_senarai_pekerja">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Nama</th>
                    <th>Jantina</th>
                    <th>Jawatan</th>
                    <th>Tarikh Mula Bekerja</th>
                    <th>Pilihan</th>
                </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>

    </div>


</div>