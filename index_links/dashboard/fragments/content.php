<!-- BEGIN DASHBOARD STATS 1-->
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-bar-chart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="permohonan_total_all_this_year">0</span>
                </div>
                <div class="desc"> Latihan</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 red" href="#">
            <div class="visual">
                <i class="fa fa-calendar"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="pembaharuan_total_all_this_year">0</span>
                </div>
                <div class="desc"> Latihan</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 blue" href="#">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="permohonan_total_today">0</span>
                </div>
                <div class="desc"> Latihan</div>
            </div>
        </a>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <a class="dashboard-stat dashboard-stat-v2 purple" href="#">
            <div class="visual">
                <i class="fa fa-gavel"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span id="pembaharuan_total_today">0</span>
                </div>
                <div class="desc"> Latihan</div>
            </div>
        </a>
    </div>
</div>
<div class="clearfix"></div>
<div class="row">
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light calendar bordered">
            <div class="portlet-title ">
                <div class="caption">
                    <i class="icon-calendar font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Kalendar</span>
                </div>
            </div>
            <div class="portlet-body">
                <div id="calendar1"></div>
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
    <div class="col-lg-6 col-xs-12 col-sm-12">
        <!-- BEGIN PORTLET-->
        <div class="portlet light bordered">
            <div class="portlet-title tabbable-line">
                <div class="caption">
                    <i class="icon-globe font-dark hide"></i>
                    <span class="caption-subject font-dark bold uppercase">Aktiviti</span>
                </div>
                <ul class="nav nav-tabs">
                    <li class="active">
                        <a href="#tab_1_1" class="active" data-toggle="tab"> Latihan
                            <a href="#tab_1_2" data-toggle="tab" style="display: none"> Lain-lain </a>

                    </li>
                    <li>
                        <a href="#tab_1_2" data-toggle="tab" style="display: none"> Lain-lain </a>
                    </li>
                </ul>
            </div>
            <div class="portlet-body">
                <!--BEGIN TABS-->
                <div id="main_activity_template" style="display:none;">
                    <li class="main-activity-item">
                    </li>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab_1_1">
                        <div class="scroller" style="height: 339px;" data-always-visible="1" data-rail-visible="0">
                            <ul class="feeds main-activity-feed">
                                <li id="id_feeds" style="display: none;">
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-info">
                                                    <i class="fa fa-clone"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"></div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab_1_2">
                        <div class="scroller" style="height: 290px;" data-always-visible="1" data-rail-visible1="1">
                            <ul class="feeds other-activity-feed">
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-warning">
                                                    <i class="fa fa-bullhorn"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> Sistem ESV3 akan diselengara pada setiap hari sabtu
                                                    jam 10 pagi
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> 5 jam</div>
                                    </div>
                                </li>
                                <li>
                                    <div class="col1">
                                        <div class="cont">
                                            <div class="cont-col1">
                                                <div class="label label-sm label-success">
                                                    <i class="fa fa-bullhorn"></i>
                                                </div>
                                            </div>
                                            <div class="cont-col2">
                                                <div class="desc"> 9 Mei 2018 diumumkan sebagai cuti khas</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col2">
                                        <div class="date"> 1 hari</div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!--END TABS-->
            </div>
        </div>
        <!-- END PORTLET-->
    </div>
</div>
<!-- END DASHBOARD STATS 1-->

<!--<script src="index_links/dashboard/js/custom.js" type="text/javascript"></script>-->