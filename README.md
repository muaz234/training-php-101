# TRAINING PHP 

### ABOUT THE PROJECT
Sample code for training to create datables with server side data and updating its content with bootstrap modal.
Created using PHP, Bootstrap Framework, JQuery and MySQL database engine

### Installation
1. Ensure Apache, PHP and MySQL service is running
2. Clone this repo
3. Export database content from this repo into mySQL engine
`https://github.com/datacharmer/test_db`
4. Update credentials at `backend/db_info.php`

### Usage

### Roadmap
If any issues found, please create a new issues at the issues tab

### Contributing
If you would like to contribute, you may follow the steps below:
1. Fork the project
2. Create a new branch
3. Commit your code changes
4. Push to the `master` branch
5. Submit a pull request
6. Send an email to [my email](ahmedmuaz0152@gmail.com)

### License

### Contact
Muaz - [my email](ahmedmuaz0152@gmail.com)

Project Link: https://gitlab.com/muaz234/training-php-101/-/tree/master/

Markdown Format Reference: https://github.com/othneildrew/Best-README-Template


