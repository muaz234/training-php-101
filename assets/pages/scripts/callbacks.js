var callbacks = {
    //WHEN MODAL IS TRIGGERED, SYSTEM WILL LOOK FOR THIS FUNCTION
    modalLoad: function (mergeModalConfig) {
        var modalLinkID = "#" + mergeModalConfig.link_id;
        var modalID = "#" + mergeModalConfig.modal_id;

        if ($("#" + mergeModalConfig.target_id).height() <= 10) { // LESS THAN 10PX DOES NOT MAKE SENSE FOR MODAL BODY
            if ($("#" + mergeModalConfig.target_id).hasClass("custom-modal-div-body")) {
                $("#" + mergeModalConfig.target_id).css("height", globalDivHeight);
            } else if ($("#" + mergeModalConfig.target_id).hasClass("custom-modal-div-body-map")) {
                $("#" + mergeModalConfig.target_id).css("height", globalDivHeightMap);
            }
        }

        $(modalID + " .custom-modal-title i").removeClass().addClass(mergeModalConfig.title_icon);
        $(modalID + " .custom-modal-title-text").html(globalTextTruncate.apply(decodeURIComponent(mergeModalConfig.title_text.replace(/%3Cbr%20%2F%3E%0A/g, ", ")), [60, true]));

        if (mergeModalConfig.type == "generic_datatables") {
            //THE CULPRIT IS HERE!!!
            var tableID = "#" + mergeModalConfig.target_id;
            $(tableID).removeClass("hidden");
            $(tableID + " tr th").remove();
            $(tableID + " thead tr").append(mergeModalConfig.table_th);
            var genericDataTable = $(tableID).dataTable({
                "bDestroy": "true",
                "sScrollY": globalDtHeight,
                "scrollX": "true"
            });

            if (mergeModalConfig.table_uppercase) {
                $(tableID).css("text-transform", "uppercase");
            }
        }

        $(modalLinkID).click();
        $(modalID).one('shown.bs.modal', function () {
            if (mergeModalConfig.type == "generic_datatables") {
                globalLoadDataTables(mergeModalConfig);
            } else if (mergeModalConfig.type == "googlemap_split") {
                custom.modalGoogleMapSplit(mergeModalConfig);
            } else if (mergeModalConfig.type == "custom_modal") { //KPI MODAL
                $(".kpi-form-item-title").removeClass("text-danger");

                if ($("#kpi_event").val() == "add_main") {
                    $(".kpi-form-item-title").removeClass("text-danger");
                    $(".kpi-category-hide").fadeOut(300);

                    kpiAdd("");

                    $(modalID + " .custom-modal-title i").removeClass().addClass("fa fa-plus-square");
                    $(modalID + " .custom-modal-title-text").html($("#kpi_add_new").val());
                    $("#kpi_mainkpi_id").val("");
                }
            }
        });

        $(modalID).one('hidden.bs.modal', function (event) {
            if (mergeModalConfig.type == "googlemap_split") {
                $("#" + mergeModalConfig.split_body_id).empty();
                $("#" + mergeModalConfig.split_pano_id).empty();
            } else if (mergeModalConfig.type == "generic_datatables") {
                $("#" + mergeModalConfig.target_id).empty();
                $("#" + mergeModalConfig.target_id).addClass("hidden");
                $(genericDataTable).dataTable().api().destroy();
            } else if (mergeModalConfig.type == "custom_modal") { //KPI MODAL
                $("#kpi_event").val("");
                $(".kpi-modalclose-reset").each(function () {
                    $("#" + this.id).val("");
                });
                $(".kpi-category-hide").fadeIn(300);
            }
        });
    },

    //WHEN AUTOCOMPLETE IS SELECTED, SYSTEM WILL LOOK FOR THIS FUNCTION
    autocompleteEvent: function (suggestion) {
        var modalData = {};

        if (suggestion.data.type == "map_location") {
            modalData['type'] = "googlemap_split";
            modalData['title_text'] = suggestion.value;
            if (suggestion.data.extras) {
                var jsonExtra = JSON.parse(suggestion.data.extras);
                modalData['lat'] = jsonExtra.lat;
                modalData['lng'] = jsonExtra.lng;
                modalData['streetview'] = jsonExtra.streetview;
            }
        } else if (suggestion.data.type == "table_location") {
            modalData['type'] = "generic_datatables";
            modalData['title_text'] = "";
            modalData['method'] = "get_list_maplocations";
            modalData['extraquery_title'] = 1;
            modalData['visibility'] = [0, 5, 6, 7, 8];
            modalData['colvis'] = [0, 7, 8];
            modalData['table_th'] = "<th>ID</th><th>Nama</th><th>Jenis</th><th>Alamat</th><th>Negeri</th><th>Lat</th><th>Lng</th><th>Fixed</th><th>StreetView</th>";
            modalData['externalRowFunction'] = "datatablesGoogleMapLink";
            if (suggestion.data.extras) {
                var jsonExtra = JSON.parse(suggestion.data.extras);
                modalData['extraquery_link_id'] = [{
                    "target_value": suggestion.value,
                    "target_query": jsonExtra.target_query
                }];
            }
        } else if (suggestion.data.type == "table_ic") {
            modalData['type'] = "generic_datatables";
            modalData['title_text'] = "STATUS UNTUK INDIVIDU";
            modalData['calltype'] = "spps";
            modalData['method'] = "get_list_ic";
            modalData['extraquery_title'] = 1;
            modalData['visibility'] = [];
            modalData['colvis'] = [];
            //modalData['totalColumns'] = 3;
            modalData['sAjaxDataProp'] = "data";
            modalData['table_th'] = "<th>ID</th><th>Negeri Memohon</th><th>Status</th><th>Jenis</th>";
            //modalData['externalRowFunction'] = "datatablesGoogleMapLink";
            if (suggestion.data.extras) {
                var jsonExtra = JSON.parse(suggestion.data.extras);
                modalData['extraquery_link_id'] = [{
                    "target_value": suggestion.value,
                    "target_query": jsonExtra.target_query
                }];
            }
        }
        globalLoadModal(modalData);
    }
}

