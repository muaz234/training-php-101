var chartFunctions = {
    loadingChart: function(id){
        $("#"+id).fadeIn(500);    
        $("#"+id).html("<div class='text-center'><img src='"+globalCustomImgPath+"logo/chart-loading-logo.png'><img class='fa faa-flash animated' src='"+globalCustomImgPath+"logo/chart-loading-chart.png'></div>");    
    },
    
    noData: function(id){
        $("#"+id).fadeIn(500);    
        $("#"+id).html("<div class='text-center'><img src='"+globalCustomImgPath+"logo/chart-loading-logo.png'> <label class='label label-danger fa faa-flash animated'>CARIAN TIADA DATA</label></div>");    
    },

    emptyChart: function(id){
        $("#"+id).fadeOut(500);    
    },

    nFormatter: function(num) {
        var isNegative = false;
        if (num < 0) {
            isNegative = true;
        }
        num = Math.abs(num)
        if (num >= 1000000000) {
            var formattedNumber = (num / 1000000000).toFixed(1).replace(/\.0$/, '') + 'B';
        } else if (num >= 1000000) {
            var formattedNumber =  (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
        } else  if (num >= 1000) {
            var formattedNumber =  (num / 1000).toFixed(1).replace(/\.0$/, '') + 'K';
        } else {
            var formattedNumber = num;
        }   
        if(isNegative) { formattedNumber = '-' + formattedNumber }
        return formattedNumber;
    },
    
    clearChartObject: function(chartid){
        var clearchart = AmCharts.charts.filter(function(v){
            return v.id == chartid;
        });
        if(clearchart.length) { clearchart[0].clear(); }        
    },
    
    decodeHTML: function(textvalue){
        var elem = document.createElement('textarea');
        elem.innerHTML = textvalue;
        var decoded = elem.value;
        return decoded;        
    },
    
    showFilterText: function(filterTextID,filterText,data,dataProvider){
        $(filterTextID).html("");
        var filterStatus = "";
        if(filterText){
            var overall_total = "";   
            if(jQuery.isEmptyObject(data)){
                filterStatus = "<label class='label label-danger'><i class='fa fa-filter'></i> NO FILTERED RECORDS FOUND</label>";
            }
            else {
                if(dataProvider[0].overall_total){
                    overall_total = "<h5>TOTAL: ("+dataProvider[0].overall_total+")</h5>";    
                }
            }                            
            $(filterTextID).html("<h5>CURRENT FILTER - "+filterText+"</h5>"+overall_total+filterStatus);                    
        }                    				        
    },
    
    responsiveChart: function(responsiveObj){
        
        if($("#"+responsiveObj.divID).width() <= chartWidthFullResponsive){
            if(responsiveObj.fontSize.global){
                responsiveObj.chartOption.fontSize = chartFontSizeResponsive;                
            }
            if(responsiveObj.fontSize.titles){
                responsiveObj.chartOption.titles[0].size = chartTitleSizeResponsive;
                responsiveObj.chartOption.titles[1].size = chartSubTitleSizeResponsive;    
            }
            if(responsiveObj.fontSize.legend){
                responsiveObj.chartOption.legend.fontSize = chartLegendFontSizeResponsive;
            }
            if(responsiveObj.fontSize.balloon){
                responsiveObj.chartOption.balloon.fontSize = chartLegendFontSizeResponsive;
            }                    
            if(responsiveObj.fontSize.balloon){
                responsiveObj.chartOption.balloon.fontSize = chartLegendFontSizeResponsive;
            } 
            if(typeof responsiveObj.emptyParams !== 'undefined'){
                for(var i = 0; i < responsiveObj.emptyParams.length; i++){
                    responsiveObj.chartOption[responsiveObj.emptyParams[i]] = "";
                }
            }   
        }
        return responsiveObj.chartOption;
    },
    
    noAccessRole: function(){
        toastr.error('Access To Details Not Allowed For '+$("#deptdiv_title").val(),'Details Access');
    },

    formatTextLabel: function(label){
        if (label.length > globalChartSubstr) {
            return label.substr(0, globalChartSubstr) + '...';
        }
        return label;
    }
}