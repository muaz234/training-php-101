var Portlet = function () {
    return {
        init: function () {

            //CHANGE CHART
            $(".custom-chart-select").click(function(element){
                var portletId = $(this).closest(".custom-main-portlet").attr("id");
                var chartType = "";
                var saveChartType = "";                

                $(this).addClass("hidden");

                if($(this).hasClass("custom-chart-select-line")){
                    chartType = "line";
                    saveChartType = "line";
                    $("#"+portletId+" .custom-chart-select-bar").removeClass("hidden");
                }
                else if($(this).hasClass("custom-chart-select-area")){
                    chartType = "area";
                    saveChartType = "area";
                    $("#"+portletId+" .custom-chart-select-line").removeClass("hidden");
                }
                else if($(this).hasClass("custom-chart-select-bar")){
                    chartType = "column";
                    saveChartType = "column";
                    $("#"+portletId+" .custom-chart-select-area").removeClass("hidden");
                }
                else if($(this).hasClass("custom-chart-select-pie")){
                    chartType = "pie";
                    saveChartType = "pie";
                    $("#"+portletId+" .custom-chart-select-donut").removeClass("hidden");
                }
                else if($(this).hasClass("custom-chart-select-donut")){
                    chartType = "donut";
                    saveChartType = "donut";
                    $("#"+portletId+" .custom-chart-select-pie").removeClass("hidden");
                }

                globalChangeChart(portletId,true,chartType,"refresh");
                
                var params = {
                    method: "get_save_portletgraphtype",
                    calltype: "portlet",
                    portletId: portletId,
                    portletGraphType: saveChartType
                };

                $.get("backend/ajax_call.php?"+$.param(params))
                .done(function( data ) {
                    //console.log(data);
                });    
            });
            
            //RESIZE
            $(".resize").click(function(element){
                var portletId = $(this).attr("data-id");
                var portletSize = "";

                if($("#"+portletId).hasClass("col-lg-12")){
                    $("#"+portletId).removeClass("col-lg-12").addClass("col-lg-6");
                    portletSize = "col-lg-6";
                }
                else if($("#"+portletId).hasClass("col-lg-6")){
                    $("#"+portletId).removeClass("col-lg-6").addClass("col-lg-12");
                    portletSize = "col-lg-12";
                }

                if($(this).attr("data-chart-type") == "mapgen" || $(this).attr("data-chart-type") == "map"){
                    globalMapObjectReference[portletId+"_mapgen"].invalidateSize(true);
                }    
                
                var params = {
                    method: "get_save_portletsize",
                    calltype: "portlet",
                    portletId: portletId,
                    portletSize: portletSize
                };
                        
                $.get("backend/ajax_call.php?"+$.param(params))
                .done(function( data ) {
                });                
            });

            //PAGE FULLSCREEN

            $('body').on('click', '.portlet > .portlet-title .fullscreen', function(e) {
                var otherObj = {};
                var thisObj = this;
                
                var portletId = $(this).attr("data-id");

                if($("#"+portletId).find(".portlet-body").css("display") == "none"){
                    $("#"+portletId).find(".expand").click();
                    setTimeout(function(){ portletFullScreen(e,thisObj,otherObj); }, 500);
                }
                else{
                    portletFullScreen(e,thisObj,otherObj);
                }
            });

            $('#clickmewow').click(function(){
                $('#radio1003').attr('checked', 'checked');
            });

            $(".custom-fullscreen-ind-button").on("click",function(){
                var portlet = $(this).closest(".custom-main-portlet").attr("id");
                var title = $("#"+portlet+" .caption-subject").html();

                /*           
                var fullScreenData = $(this).attr("data-fullscreen");                
                var fullscreenDataObj = JSON.parse(fullScreenData);     
                var portletId = fullscreenDataObj.portlet_id;               
                var source_attr = fullscreenDataObj.source_attr;
                var chart_id = fullscreenDataObj.chart_id;                
                var chartConfigData = $("#"+chart_id).attr(source_attr);
                */
                var portletId = $(this).closest(".custom-main-portlet").attr("id");
                var source_attr = "data-chartattr";
                var chart_id = portletId+"_body";                
                var chartConfigData = $("#"+chart_id).attr(source_attr);
                var modal_title = $("#"+portlet+" .caption-subject").html();

                var chartConfig = JSON.parse(chartConfigData);
                var portletChartClick = JSON.parse($("#"+chart_id).attr("data-chartclick"));
                var portletQueryString = JSON.parse($("#"+chart_id).attr("data-querystring"));            
                chartConfig['querystring'] = portletQueryString;
                chartConfig['chartclick'] = portletChartClick;
                chartConfig['changeChart'] = false;
                //chartConfig['sourceId'] = fullscreenDataObj.chart_id;
                chartConfig['sourceId'] = chart_id;
                chartConfig['eventType'] = "refresh";                
            
                var dataObj = globalChartObjects[chart_id];

                $("#"+globalModalFullScreenAttr.linkId).click();
                $("#"+globalModalFullScreenAttr.modalId).one('shown.bs.modal', function (event) {
                    globalModalFullscreen = true;
                    //$("#"+globalModalFullScreenAttr.modalId+" .custom-modal-title-text").html($(".caption-subject-dynamic:first").text()+" "+fullscreenDataObj.modal_title);
                    $("#"+globalModalFullScreenAttr.modalId+" .custom-modal-title-text").html($(".caption-subject-dynamic:first").text()+" "+modal_title);
                    globalGenerateChart(portletId,chartConfig,dataObj);
                 
                });
                
                $("#"+globalModalFullScreenAttr.modalId).one('hidden.bs.modal', function (event) {
                    globalModalFullscreen = false;
                });    
            });                   

            //COLLAPSE EXPAND
            $(".collapse").on('click',function(e){
                
                var portletId = $(this).attr("data-id");
                var portletExpand = 1;

                if($(this).attr("class") == "collapse"){
                    portletExpand = 0;
                }

                var params = {
                    method: "get_save_portletexpand",
                    calltype: "portlet",
                    portletId: portletId,
                    portletExpand: portletExpand
                }

                $.get("backend/ajax_call.php?"+$.param(params))
                .done(function( data ) {
                    //alert(data);
                });                                
            });

            //SORTABLE DASHBOARD
            $( "#sortable_portlets" ).sortable({
                connectWith: ".portlet",
                opacity: 0.7,
                containment: "parent",
                handle: ".caption",
                coneHelperSize: true, //NEW
                placeholder: "portlet-sortable-placeholder",
                tolerance: "pointer", //UPDATED FROM intersect                
                helper: "clone",
                forcePlaceholderSize: !0, //NEW
                cancel: ".portlet-sortable-empty, .portlet-fullscreen", // NEW cancel dragging if portlet is in fullscreen mode
                delay: 150,
                update: function(b, c) {
                    if (c.item.prev().hasClass("portlet-sortable-empty")) {
                        c.item.prev().before(c.item);
                    }                    
                }
            });       
              
            //SAVE DASHBOARD
            $( ".row" ).on( "sortstop", function( event, ui ) {
                if(event.target.tagName.toLowerCase() == "div"){ //ONLY SAVE FOR PORTLETS, NOT REPORT
                    var sortedIDs = $( ".row" ).sortable( "toArray" );

                    $.post( "backend/ajax_call.php", { 
                        method: "post_save_dashboard",
                        calltype: "portlet",
                        ids: JSON.stringify(sortedIDs)
                    })
                    .done(function( data ) {
                        //console.log(data);
                    });                
                }
            });

            //DRAGGABLE MODAL
            $('.modal').draggable({
                handle: ".custom-modal-header"
            });              

            //CAPTURE ALL PORTLET IDs INITIAL STATE - FOR RESET AND SEND PURPOSES
            var portletSwitchesInitial = {};

            $('.make-switch').each(function () {
                portletSwitchesInitial[this.id] = $('#'+this.id).bootstrapSwitch('state');
            });

            $(".custom-portletselect").click(function(){
                portletSelectStatus($(this).attr("data-value"));
            });

            //EXPAND AND COLLAPSE PORTLET SELECTION BY DEFAULT
            $(".custom-panel-expand").click();

            $(".custom-toggle-portlet-panel").on('click',function(e){
                if($(this).hasClass("in")){
                    $(this).removeClass("in");
                    $(this).removeClass("fa-minus-square").addClass("fa-plus-square");
                    $('.custom-panel-group .panel-collapse').each(function(i, item) {
                        var panelId = $(item).attr("id");
                        if($(item).hasClass("in")){
                            $("."+panelId).click();
                        }                        
                    });
                }                
                else{
                    $(this).addClass("in");
                    $(this).removeClass("fa-plus-square").addClass("fa-minus-square");
                    $('.custom-panel-group .panel-collapse').each(function(i, item) {
                        var panelId = $(item).attr("id");
                        if(!$(item).hasClass("in")){
                            $("."+panelId).click();
                        }                        
                    });
                }                
            });

            $(".custom-toggle-portlet-on").on('click',function(e){
                $(".custom-switch").each(function(index) {
                    $(this).find(".bootstrap-switch-off").find(".make-switch").click();
                });                
            });    

            $(".custom-toggle-portlet-off").on('click',function(e){
                $(".custom-switch").each(function(index) {
                    $(this).find(".bootstrap-switch-on").find(".make-switch").click();
                });                
            });    

            $(".custom-toggle-portletgroup-on").on('click',function(e){
                $(this).closest(".panel-primary").find(".bootstrap-switch-off").find(".make-switch").click();
            });    

            $(".custom-toggle-portletgroup-off").on('click',function(e){
                $(this).closest(".panel-primary").find(".bootstrap-switch-on").find(".make-switch").click();           
            });             
            
            function portletSelectStatus(status){
                if(status == "cancel"){
                    for (var key in portletSwitchesInitial) {
                      $('#'+key).bootstrapSwitch('state',portletSwitchesInitial[key]);
                    }        
                }
                else if(status == "save"){
                    swal({
                        title: $("#custom_swal_areyousure").val(),
                        text: $("#custom_swal_reloadtext").val(),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: $("#custom_swal_proceed").val(),
                        cancelButtonText: $("#custom_swal_cancel").val(),
                    }).then((result) => {
                        if (result.value) {
                            globalReloadPage();

                            var portletSwitchesSave = {};
                            var portletStatus = 0;
        
                            $('.make-switch').each(function () {
                                if($('#'+this.id).bootstrapSwitch('state') == true){
                                    portletStatus = 1;
                                }
                                else{
                                    portletStatus = 0;    
                                }
                                portletSwitchesSave[$('#'+this.id).attr('data-id')] = portletStatus;
                            });              
        
                            
                            $.post( "backend/ajax_call.php", { 
                                method: "post_save_portletstat",
                                calltype: "portlet",
                                portlet_ids: JSON.stringify(portletSwitchesSave)
                            })
                            .done(function( data ) {
                                location.reload();
                            });                        
                        }
                    });                               
                }
                else if(status == "original"){
                    swal({
                        title: $("#custom_swal_areyousure").val(),
                        text: $("#custom_swal_reloadtext").val(),
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonText: $("#custom_swal_proceed").val(),
                        cancelButtonText: $("#custom_swal_cancel").val(),
                    }).then((result) => {
                        if (result.value) {
                            globalReloadPage();

                            var params = {
                                method: "get_reset_dashboard",
                                calltype: "portlet"
                            };
                                
                            $.get("backend/ajax_call.php?"+$.param(params))
                            .done(function( data ) {
                                location.reload();
                            });        
                        }
                    });                    
                }
            } 

            function portletFullScreen(e,thisObj,otherObj){
                e.preventDefault();  
                            
                var portletId = $(thisObj).attr("data-id");
                var oriHeight = "";
                
                if($(thisObj).hasClass("fullscreen on")){
                    $("#"+portletId+" .resize").addClass("hidden");
                }
                else{
                    $("#"+portletId+" .resize").removeClass("hidden");    
                }

                var portlet = $(thisObj).closest(".portlet");
                var portletBody = $("#"+portletId+" .content-fullscreen");

                if(portlet.hasClass('portlet-fullscreen')) {                  
                    oriHeight = $("#"+portletId+" .content-fullscreen").css("height");

                    var portletBodyHeight = $("#"+portletId+" .portlet-body").height();
                    var customObj = $("#"+portletId+" .custom-content-area");
                    var customHeight = globalCalculateCssHeight([customObj],false);
                    var contentHeight = portletBodyHeight - customHeight; 

                    portletBody.attr("data-oriheight",oriHeight);
                    portletBody.css("height",contentHeight+"px");
                    $("#"+portletId+" .custom-chart-select").addClass("display-hide-tablet");
                }                           
                else{
                    oriHeight = portletBody.attr("data-oriheight");
                    portletBody.css("height",oriHeight);
                    $("#"+portletId+" .custom-chart-select").removeClass("display-hide-tablet");
                }

                if($(thisObj).attr("data-chart-type") == "map"){
                    globalMapObjectReference[portletId+"_mapgen"].invalidateSize(true);
                    if(!$("#"+portletId+" #custom_map_select_state").val()){
                        $("#"+portletId+" .custom-map-refresh").click();
                    }
                }                                              
                if($(thisObj).attr("data-chart-type") == "mapgen"){
                    globalMapObjectReference[portletId+"_mapgen"].invalidateSize(true);
                    if(!$("#"+portletId+" #generator_mapgen_state_select").val()){
                        $("#"+portletId+" .custom-map-refresh").click();
                    }
                }
            }
        },
    };
}();