/* GLOBAL VALUES */
/*
var generalSettings;
$.ajax({
    async: false,
    url: "../spkms/backend/ws_pengurusan_kes.php?method=get_general_settings",
}).done(function(data){
    generalSettings = JSON.parse(data);
});
*/
var globalCustomerName =  $("#custom_dash_customername").val();
var globalWindowHeight = $(window).height();
var globalDtHeight = Math.floor(globalWindowHeight * 0.40);
var globalDivHeight = Math.floor(globalWindowHeight * 0.50);
var globalDivHeightMap = Math.floor(globalWindowHeight * 0.70);

var globalMobileWidth = 580;
var globalMobileHeight = 500;
var globalTabletWidth = 991;
var globalDesktopWidth = 1920;
var globalLargeScreenWidth = 1980;
var globalArrColorScheme = ["#35478C","#4E7AC7","#5926FF","#648CBD","#FF312D","#001B73","#1D60B4","#20579A","#2185C5","#334B8C","#4E74D9","#496DCC","#405FB2","#005EA4","#0084E7","#003B67","#008CF4","#0076CD","#CE749E","#C43578","#912759","#FE3559","#8C195D","#D92690","#CC2487","#B22076","#E21370","#A20E50","#620831","#EF1476","#C81163","#555555","#666666","#777777","#888888","#999999","#333333","#F62E04","#C84024","#F7A10B","#C6830E","#F3842E","#D0620C","#FF7F50","#FF6347","#FF4500","#FFB603","#FFA500","#FF8C00"];

var globalMaxFileUpload = 10485760;
//var globalMaxFileUpload = parseInt(generalSettings.muatnaik_saiz_limit_bytes);
var globalFileExtApproved = ["doc","docx","xls","xlsx","ppt","pptx","txt","pdf","csv","zip","rar","7zip","jpg","jpeg","gif","png"];
//var globalFileExtApproved = generalSettings.muatnaik_format_jenis.split(',');
var globalFileExtImage = ["jpg","jpeg","gif","png"];
//var globalFileExtImage = generalSettings.muatnaik_imej_jenis.split(',');

var globalMaxGeneratorRecords = 10000;

var globalCountryName = "Malaysia";
var globalCountryCode = "127";

//SAVE ALL OPENED CHILD WINDOWS
var globalOpenedWindows = [];

//DETECT DESKTOP, TABLET OR MOBILE
var globalDeviceMobile = false;
var globalDeviceTablet = false;
var globalDeviceLaptop = false;
var globalDeviceLarge = false;

if($(window).width() <= globalMobileWidth || $(window).height() <= globalMobileHeight){
    globalDeviceMobile = true;
}
else if($(window).width() <= globalTabletWidth){
    globalDeviceTablet = true;
}
else if($(window).width() <= globalDesktopWidth){
    globalDeviceLaptop = true;
}
else if($(window).width() <= globalLargeScreenWidth){
    globalDeviceLarge = true;
}
else{
    //toastr.error("Viewport Not Supported. Contact Admin","VIEWPORT SUPPORT");
    globalDeviceLarge = true;
}

//MODAL FULLSCREEN STATE
var globalModalFullscreen = false;
var globalModalExpandWidth = ($(window).width()-20)+"px";

var globalModalFullScreenAttr = {
    "linkId":"fullscreen_div_list_link",
    "modalId":"fullscreen_div_list_modal",
    "divId":"fullscreen_div_list_body",
    "components":["#fullscreen_div_list_modal",".custom-modal-body",".custom-modal-portlet-box",".custom-modal-header",".custom-modal-portlet-body",".custom-modal-footer"]
};

//QUILL RTE
var globalQuillRTEOptions = [
    ['bold', 'italic', 'underline', 'strike'],        
    ['link'],
    [{ 'header': 1 }, { 'header': 2 }],               
    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    [{ 'indent': '-1'}, { 'indent': '+1' }],          
    [{ 'size': ['small', false, 'large', 'huge'] }], 
    [{ 'align': [] }],
    ['clean']                                         
];

var globalQuillEmptyText = "<p><br></p>";
//Quill Empty Detect Example 
// quillS_laporan_sulh.editor.isBlank(); //true and false

//FEEDBACK MODAL
var feedbackEditorId = "custom_feedback_editor";
if(document.getElementById(feedbackEditorId)){
    if (typeof Quill != "undefined") {
        var toolbarOptions = globalQuillRTEOptions;

        var quillFeedback = new Quill(document.getElementById(feedbackEditorId), {
            modules: {
                toolbar: toolbarOptions
            },
            placeholder:$("#feedback_placeholder").val(),
            theme: 'snow'  // or 'bubble'
        });    
    }        
}

globalLandscapeAlert();
//$("#"+globalModalFullScreenAttr.divId).css("height",globalWindowHeight-globalCalculateCssHeight(globalModalFullScreenAttr.components,true)+"px");

$(window).resize(function(){
    //REDRAW DATATABLES ON WINDOW RESIZE IF OBJECT EXISTS
    if(Object.keys(globalDTObjectReference).length > 0){
        setTimeout(function(){
            $.fn.dataTable.tables( {visible: true, api: true} ).columns.adjust();
        }, 1000);   
    }

    globalWindowHeight = $(window).height();
    globalDtHeight = Math.floor(globalWindowHeight * 0.40); //DATATABLES RESIZE
    globalDivHeight = Math.floor(globalWindowHeight * 0.50);      
    globalDivHeightMap = Math.floor(globalWindowHeight * 0.70);   

    globalDeviceMobile = false;
    globalDeviceTablet = false;
    globalDeviceLaptop = false;
    globalDeviceLarge = false;
    
    if($(window).width() <= globalMobileWidth || $(window).height() <= globalMobileHeight){
        globalDeviceMobile = true;
    }
    else if($(window).width() <= globalTabletWidth){
        globalDeviceTablet = true;
    }
    else if($(window).width() <= globalDesktopWidth){
        globalDeviceLaptop = true;
    }
    else if($(window).width() <= globalLargeScreenWidth){
        globalDeviceLarge = true;
    }
    else{
        globalDeviceLarge = true;
    }   

    globalLandscapeAlert();
    //$("#"+globalModalFullScreenAttr.divId).css("height",globalWindowHeight-globalCalculateCssHeight(globalModalFullScreenAttr.components,true)+"px");
});

function globalLandscapeAlert(){
    var windowHeight = $(window).height();
    var windowWidth = $(window).width();
    if(windowHeight > windowWidth){
        toastr.warning('Landscape Mode Has Better Visualization','MOBILITY');        
    }
}

function globalCalculateCssHeight(arrComponents,height){
    var totalHeight = 0;

    for(var i=0; i < arrComponents.length; i++){
        totalHeight += parseInt($(arrComponents[i]).css('padding-top'))+parseInt($(arrComponents[i]).css('padding-bottom'))+parseInt($(arrComponents[i]).css('margin-top'))+parseInt($(arrComponents[i]).css('margin-bottom'))+parseInt($(arrComponents[i]).css('border-top'))+parseInt($(arrComponents[i]).css('border-bottom'));

        if(height){
            totalHeight += parseInt($(arrComponents[i]).height())
        }
    } 
    return totalHeight+10;
}

var globalMonthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var globalGMT = "08:00";

//NETWORK COUNTER
var globalNoNetworkCounterLimit = 5;

//MOMENT DATE START
$.fn.dataTable.moment( 'D MMM YYYY' ); // DATATABLES DATE SORT
var globalSqlDTFormat = "YYYY-MM-DD HH:mm:ss"; //SQL DATETIME FORMAT
var globalSqlDFormat = "YYYY-MM-DD"; //SQL DATE FORMAT
var globalWebDTFormat = "DD/MM/YYYY hh:mm A"; //WEB DATETIME FORMAT
var globalWebDFormat = "DD/MM/YYYY"; //WEB DATE FORMAT

function globalWebDateTime(datetime){
    if(datetime){
        return moment(datetime,globalSqlDTFormat).format(globalWebDTFormat);
    }
    return datetime;
}
function globalWebDate(date){
    if(date){
        return moment(date,globalSqlDFormat).format(globalWebDFormat);
    }
    return date;
}
function globalSqlDateTime(datetime){
    if(datetime){
        return moment(datetime,globalWebDTFormat).format(globalSqlDTFormat);
    }
    return datetime;    
}
function globalSqlDate(date){
    if(date){
        return moment(date,globalWebDFormat).format(globalSqlDFormat);
    }
    return date;
}

var globalMomentStartHour = 9;
var globalMomentStartMinute = 0;
var globalMomentStartTime = "09:00:00";
//MOMENT DATE END

//DATATABLES ERROR HANDLING (IF REQUIRED)
/*
$.fn.dataTable.ext.errMode = function ( settings, helpPage, message ) { 
    console.log(message);
};
*/

//DATATABLE LANGUAGE
var globalDtLanguage = "en";

//BLOCKUI DEFAULT CSS
var globalBlockUICSS = {
    width: "500px",
    background: "rgba(0, 0, 0, 0.7)",
    padding: "20px",
    color: "#ffffff",
    border: "2px solid rgba(255, 150, 0, 0.8)",
    'border-radius': "5px !important",
    'line-height': "2.2rem",
    '-webkit-box-shadow': "0px 0px 50px 10px rgba(255, 255, 255, 0.3)",
    '-moz-box-shadow': "0px 0px 50px 10px rgba(255, 255, 255, 0.3)",
    'box-shadow': "0px 0px 50px 5px rgba(255, 203, 110, 0.4)"
};

//CUSTOM PATHS
var globalCustomImgPath = '../common/assets/global/img/';
var globalCustomPluginPath = '../common/assets/global/plugins/';

//RESPONSE TIME VARIABLES
var globalFastResponse = 0.2;
var globalAverageResponse = 0.5;
var globalSlowResponse = 1.5;
var globalSpeedTooltip = $(".custom-speedicon-fast").attr("title");

//JAVASCRIPT AMCHARTS GLOBAL OPTIONS
var globalChartConfigs = [];
var globalChartObjects = [];
var globalChartStartDuration = 0.3;
var globalChartTitles = [{"text":""},{"text":""}];
var globalChartColor = "#555555";
var globalChartStartEffect = "easeOutSine";
var globalChartPlotAreaFillColors = "#EBECFF";
var globalChartPlotAreaFillAlphas = 0.2;
var globalChartBackgroundColor = "#EBECFF";
var globalChartBackgroundAlpha = 0;
var globalChartBorderColor = "#CCCCCC";
var globalChartBorderAlpha = 0;
var globalChartSequencedAnimation = false;
var globalChartAutoDisplay = true;
var globalChartPrefixesOfBigNumbers = [{number:1e+3,prefix:"k"},{number:1e+6,prefix:"M"},{number:1e+9,prefix:"B"}];
var globalChartUsePrefixes = true;
var globalChartDepth3DBar = 30;
var globalChartAngleBar = 20;
var globalChartDepth3DPie = 20;
var globalChartAnglePie = 60;
var globalDataDateFormat = "YYYY-MM-DD";
var globalChartLaptopSubstr = 1000; //PRACTICALLY NO TEXT TRUNCATE
var globalChartMobileTabletSubstr = 3;
var globalChartSubstr = globalChartLaptopSubstr;
var globalChartFontSize = 10.5;
var globalPieTickColor = "#000000";
var globalPieChartLegend = false;
var globalPieOutlineColor = "#FFFFFF";
var globalChartLabelColor = "#000000";
var globalMouseWheelChartZoom = true;

if(globalDeviceMobile || globalDeviceTablet){
    globalChartSubstr = globalChartMobileTabletSubstr;
    globalChartTitles = [{"text":""},{"text":""}];
    globalPieChartLegend = false;
    globalChartFontSize = 9;
}
else if(globalDeviceLarge){
    globalChartTitles = [{"text":""},{"text":""}];
    globalChartFontSize = 14;    
}

//GLOBAL COLORS
var globalColorBlue = "#1e5ac8";
var globalColorRed = "#f11725";
var globalColorGray = "#777777";
var globalColorOrange = "#FF6C00";
var globalColorGreen = "#228B22";
var globalColorYellow = "#FFA500";
var globalColorPurple = "#8E44AD";

//MAP
var globalMapAnimationDuration = 2.0;
var globalMapContentOwner = $("#custom_dash_customername").val();
var globalMapContentOwnerURL = $("#custom_dash_customerurl").val();
var globalMapStandardZoom = 13;
var globalMaxRadius = 800;
var globalZoomFactor = 10;
var globalMapFullScreenHeightRatio = 1;
var globalGeoJSONDirectory = "../../common/includes/geojson/";
var globalMapObjectReference = {};

//LAZY LOADED STATE
var globalLazyLoaded = false;

//TOASTR
var globalToastrTimeoutDefault = 5000;
var globalToastrTimeoutExport = 300000;
toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": true,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": true,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": globalToastrTimeoutDefault,
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
}

//START THEMES
if($("#custom_dash_user_theme").val()){
    var globalTheme = $("#custom_dash_user_theme").val();
    var globalThemeBrightness = "";
    
    if(globalTheme.indexOf("morning") >= 0){
        globalTheme = "blue";
        globalThemeBrightness = "light";
    }
    else if(globalTheme.indexOf("vibrant") >= 0){
        globalTheme = "vibrant";
        globalThemeBrightness = "dark";
    }
    else if(globalTheme.indexOf("grey") >= 0){
        globalTheme = "grey";
        globalThemeBrightness = "dark";
    }
    else{
        globalTheme = "default";
        globalThemeBrightness = "light";
    }
    
    if(globalDeviceLarge && (globalTheme != "vibrant" && globalTheme != "grey")){
        toastr.error("Change Theme to VIBRANT or GRAY AREA","LARGE SCREEN DETECTED!");
        setInterval(function(){
            if(globalDeviceLarge && (globalTheme != "vibrant" && globalTheme != "grey")){
                toastr.error("Change Theme to VIBRANT or GRAY AREA","LARGE SCREEN DETECTED!");
                $(".custom-dropdown-theme-select").click();
            }    
        }, 
        15000);    
    }
}
//END THEMES

//BEGIN DATATABLES STANDARD OPTIONS
var globalDtStandardOptions = {
    "bDestroy": "true",
    "sAjaxDataProp": "",
    "bProcessing": "true",
    "scrollX": "100%",
    "bDeferRender": true,
    "bAutoWidth": false,
    "pagingType": "full_numbers",
    //"dom": '<"top"Blf<"clear">>rt<"bottom"pi<"clear">>', //REMOVED l FOR HIDDEN PAGINATION DROPDOWN OPTION
    "dom":"<'row'<'col-sm-9'Bl><'col-sm-3'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'p>>",
    "aLengthMenu": [[10, 25, 50, 100, -1], [10, 25, 50, 100, $("#datatables_all").val()]],
    "sAjaxSource": "",
    "aoColumnDefs": "",    
    "buttons": [
        {
            extend: 'colvis',
            text: 'Kolum',
            className: 'custom-dt-btn-info',
            columns: ':not(.noVis)'
        },
        {
            extend: 'copy',
            className: 'custom-dt-btn-info',
            exportOptions: {
                columns: ':visible'
            }                
        },
        {
            extend: 'csv',
            className: 'custom-dt-btn-info',
            exportOptions: {
                columns: ':visible'
            }                
        },
        {
            extend: 'excel',
            className: 'custom-dt-btn-info',
            exportOptions: {
                columns: ':visible'
            }                
        },
        {
            extend: 'pdf',
            className: 'custom-dt-btn-info',
            orientation: 'landscape',
            exportOptions: {
                columns: ':visible',
                stripNewlines: false
            }                
        },
        {
            extend: 'print',
            text: 'Cetak',
            className: 'custom-dt-btn-info',
            exportOptions: {
                columns: ':visible',
                stripNewlines: false
            }                
        }
    ]
};

var globalDTObjectReference = {};

//END DATATABLES STANDARD OPTIONS

//FUNCTIONS
function globalRandomColor(singleColor){
    var lumCurrentBg = chroma($(".panel .panel-body").first().css("background-color")).luminance();
    var lumRangeMin = 0.23;
    var lumRangeMax = 0.5;

    var chromaColor = chroma.scale(globalArrColorScheme);
    
    var darkBrightMin = 0.1;
    var darkBrightMax = 1;

    if((lumCurrentBg) < lumRangeMin){ //DARK BACKGROUND
        return chroma(chromaColor(Math.random())).saturate(Math.random() * (1-0.8)+0.8).brighten(Math.random() * (darkBrightMax-darkBrightMin)+darkBrightMin).hex();
    }
    else if((lumCurrentBg) > lumRangeMax){ //LIGHT BACKGROUND
        return chroma(chromaColor(Math.random())).saturate(Math.random() * (1-0.8)+0.8).darken(Math.random() * (darkBrightMax-darkBrightMin)+darkBrightMin).hex();
    }

    return chroma(chromaColor(Math.random())).saturate(Math.random() * (1-0.8)+0.8).hex();
}

function globalSingleColor(color){
    var chromaColor = chroma(color);
    return chroma(chromaColor.luminance(Math.floor((Math.random() * 5) + 1)/10)).darken(Math.floor((Math.random() * 25) + 10)/10).hex();
}

function globalFuncRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}

function globalReloadPage(){
    $(".preloader-overlay").fadeIn(50,function(){
        $(".preloader-content").fadeIn(500);    
    });
}

function globalSpeedTest(timeout,callback){
    var startTime = performance.now();
                    
    $.ajax({
        type: 'GET',
        timeout: timeout,
        url: "backend/ajax_call.php?calltype=poll&method=poll_speed",
        success:function(data){   
            if( !data || data == ""){
                $("#custom_nonetwork_counter").val(parseInt($("#custom_nonetwork_counter").val())+1);
                $(".custom-speedicon").fadeOut(50,function(){
                    $(".custom-speedicon-none").fadeIn(50,function(){
                        if($("#custom_nonetwork_counter").val() >= globalNoNetworkCounterLimit){
                            $(".preloader-overlay-nonetwork").fadeIn(500);
                        }
                    });    
                });
            }
            else{                                
                try {
                    var JSONdataSpeed = jQuery.parseJSON(data);
                } 
                catch (e) {
                    $("#custom_nonetwork_counter").val(parseInt($("#custom_nonetwork_counter").val())+1);
                    $(".custom-speedicon").fadeOut(50,function(){
                        $(".custom-speedicon-none").fadeIn(50,function(){
                            if($("#custom_nonetwork_counter").val() >= globalNoNetworkCounterLimit){
                                $(".preloader-overlay-nonetwork").fadeIn(500);
                                $("#custom_network_status_flag").val(0);
                            }
                        });    
                    });
                }                                
                if(JSONdataSpeed.status == "success"){
                    $("#custom_nonetwork_counter").val(0);
                    if(JSONdataSpeed.lastupdate){
                        $(".custom-title-notification-text").text(JSONdataSpeed.lastupdate);  
                    }
                    
                    $(".preloader-overlay-nonetwork").fadeOut(500,function(){
                        if($("#custom_network_status_flag").val() == 0){
                            $("#custom_network_status_flag").val(1);
                            toastr.success($("#custom_dash_network_langtoastr_statustext").val(),$("#custom_dash_network_langtoastr_status").val());
                        }
                    });
                    
                    var endTime = performance.now();
                    var duration = Math.round(((endTime - startTime)/1000) * 10) / 10; //SECONDS
                    if(duration < globalFastResponse){    
                        $(".custom-speedicon").fadeOut(50,function(){
                            $(".custom-speedicon-fast").fadeIn(500);                            
                            $(".custom-speedicon-fast").attr("data-original-title",globalSpeedTooltip+" ("+duration+"S)");
                        });
                    }
                    else if(duration >= globalFastResponse && duration <= globalAverageResponse){                                    
                        $(".custom-speedicon").fadeOut(50,function(){
                            $(".custom-speedicon-normal").fadeIn(500);                            
                            $(".custom-speedicon-normal").attr("data-original-title",globalSpeedTooltip+" ("+duration+"S)");
                        });
                    }
                    else if(duration > globalAverageResponse && duration <= globalSlowResponse){                                    
                        $(".custom-speedicon").fadeOut(50,function(){
                            $(".custom-speedicon-slow").fadeIn(500);
                            $(".custom-speedicon-slow").attr("data-original-title",globalSpeedTooltip+" ("+duration+"S)");
                        });
                    }                            
                    callback("done");
                }
                else{
                    $("#custom_nonetwork_counter").val(parseInt($("#custom_nonetwork_counter").val())+1);
                    $(".custom-speedicon").fadeOut(50,function(){
                        $(".custom-speedicon-none").fadeIn(500,function(){
                            if($("#custom_nonetwork_counter").val() >= globalNoNetworkCounterLimit){
                                $(".preloader-overlay-nonetwork").fadeIn(500);
                                $("#custom_network_status_flag").val(0);
                            }
                        });                            
                    });
                    callback("done");
                }
            }
        },                        
        error:function (xhr, ajaxOptions, thrownError){
            $("#custom_nonetwork_counter").val(parseInt($("#custom_nonetwork_counter").val())+1);
            $(".custom-speedicon").fadeOut(50,function(){
                $(".custom-speedicon-none").fadeIn(500,function(){
                    if($("#custom_nonetwork_counter").val() >= globalNoNetworkCounterLimit){
                        $(".preloader-overlay-nonetwork").fadeIn(500);
                        $("#custom_network_status_flag").val(0);
                    }
                });                            
            });
            callback("done");
        }                               
    });    
}

function globalGenerateTableOptions(param){      
    if(typeof _.merge === "function"){ //LODASH FOR MOST OF THE FEATURES
        var mergeStandardConfig = _.merge({}, globalDtStandardOptions, param);
    }
    else if(typeof _.extend === "function"){ //UNDERSCORE FOR QUERY GENERATOR ONLY
        var mergeStandardConfig = _.extend({}, globalDtStandardOptions, param);
    }
    else{
        swal("FATAL ERROR","UNDERSCORE OR LODASH NOT LOADED. CONTACT ADMINISTRATOR","error");
    }    

    if(!param.sScrollY){
        mergeStandardConfig.sScrollY = globalDtHeight;
    }
    else if(param.sScrollY > 0){
        mergeStandardConfig.sScrollY = param.sScrollY;
    }

    if(param.id){
        mergeStandardConfig.initComplete = function(settings, json) {
            globalExpandCollapsePortlet(param.id);
        }
    }
    
    return mergeStandardConfig;           
}

function globalLoadModal(data){
    var genericObj = jQuery.extend(true, {}, modalDataObjects['options'][data.type]);
    if(typeof _.merge === "function"){ //LODASH FOR MOST OF THE FEATURES
        var mergeModalConfig = _.merge({}, genericObj, data);
    }
    else if(typeof _.extend === "function"){ //UNDERSCORE FOR QUERY GENERATOR ONLY
        var mergeModalConfig = _.extend({}, genericObj, data);
    }
    else{
        swal("FATAL ERROR","UNDERSCORE OR LODASH NOT LOADED. CONTACT ADMINISTRATOR","error");
    }
    
    callbacks.modalLoad(mergeModalConfig);   
}
          
function globalLoadDataTables(data){
    var tableID = "#"+data.target_id;

    var params = {
        method: "get_datatables",
        query: data.method,
        calltype: data.calltype
    };

    //ADD backend_key for Backend Detection and Processing
    if(data.backend_key){
        params.backend_key = data.backend_key;
    }

    //CHANGES MADE FOR ELECTORALS
    if(data.electorals){
        if(data.query){
            params.query = data.query;
        }        
        params.electoralquery = data.electoralquery;
    }

    //ADD AUTO GENERATED CHART DETECTION
    if(data.chart_id){
        params.chart_id = data.chart_id;
    }
    if(data.db){
        params.db = data.db;
    }
    if(data.db_others){
        params.db_others = data.db_others;
    }
    if(data.stackvalue){
        params.stackvalue = data.stackvalue;
    }
    //ADD GENERATION ENGINE QUERY
    if(data.chart_sql){
        params.chart_sql = data.chart_sql;
    }
    /*
    //MOVE THIS TO POST INSTEAD
    if(data.arr_chart_fields){
        params.arr_chart_fields = data.arr_chart_fields;
    }
    */

    //BEGIN NEW DATATABLE METHOD
    if(data.tablefilter){
        var tablefilterObj = JSON.parse(data.tablefilter);
        //MOVE THIS TO POST INSTEAD
        /*
        params.drilldown = tablefilterObj['drilldown'];
        params.drilldown_data = tablefilterObj['drilldown_data'];
        */
    }
    //END NEW DATATABLE METHOD

    //GET EXTRA QUERIES IF ANY FOR THE PARAMS

    if(data.extraquery_link_id){
        //BEGIN NEW DATATABLE METHOD
        params.extraquery = true;
        params.extraquery_data = {};
        params.extraquery_data.filters = {};
        //END NEW DATATABLE METHOD

        var extraTitle = "";
        var value = "";
                    
        for(var i = 0; i < data.extraquery_link_id.length; i++){
            if(data.extraquery_link_id[i].target_value){
                value = data.extraquery_link_id[i].target_value;
            }
            else if(data.extraquery_link_id[i].target_val_id){
                value = $("#"+data.extraquery_link_id[i].target_val_id).val();
            }
            var param = data.extraquery_link_id[i].target_query;
            if(data.extraquery_title){
                extraTitle += " "+value;
            }
            //BEGIN NEW DATATABLE METHOD
            params.extraquery_data.filters[param] = value;
            //END NEW DATATABLE METHOD
        }

        if(data.stackvalue){
            extraTitle += " "+data.stackvalue;
        }

        $("#generic_dt_list_modal .custom-modal-title-text").text(data.title_text+extraTitle);
    }

    //LOGIC ADDED FOR ELECTORALS TABLE
    if(data.method == "noquery"){
        var objParam = {
            aoColumnDefs: []
        };                       
    }
    else{
        var objParam = {
            sAjaxSource:"backend/ajax_call.php?"+$.param(params),
            sServerMethod: "POST", //ADDITIONAL PARAMETER FOR POST
            aoColumnDefs: [],
            fnRowCallback: 
                function( nRow, aData, iDisplayIndex ) {
                    if($('td a',nRow).attr("data-toggle") == "tooltip"){
                        $('td a', nRow).tooltip();
                    }
                    else if($('td a',nRow).attr("data-toggle") == "popover"){
                        $('td a', nRow).popover();
                    }
                    return nRow;                                            
                },
            fnServerData: 
                function ( sSource, aoData, fnCallback, oSettings ) {
                    if(data.arr_chart_fields){
                        aoData.push({"name": "arr_chart_fields", "value": data.arr_chart_fields});
                    }
    
                    if(tablefilterObj){
                        aoData.push({"name": "drilldown", "value": tablefilterObj['drilldown']});
                        aoData.push({"name": "drilldown_data", "value": JSON.stringify(tablefilterObj['drilldown_data'])});
                    }
                    
                    oSettings.jqXHR = $.ajax( {
                    "dataType": 'json',
                    "type": "POST",
                    "url": sSource,
                    "data": aoData,
                    "success": fnCallback
                });
            }                
        }
    }

    if(data.order){
        objParam.order = [data.order];
    }
    if(data.visibility){
        objParam.aoColumnDefs.push({"bVisible": false, "bSearchable": false, "bSortable": true, "aTargets": data.visibility});
    }
    if(data.colvis){
        objParam.aoColumnDefs.push({"className": 'noVis', "aTargets": data.colvis});
    }
    if(data.aLengthMenu){
        objParam.aLengthMenu = data.aLengthMenu;
    }
    if(data.serverSide){
        objParam.serverSide = data.serverSide;
    }
    if(data.sAjaxDataProp){
        objParam.sAjaxDataProp = data.sAjaxDataProp;
    }    
    if(data.enable_disable >= 0){
        objParam.aoColumnDefs.push({"aTargets": [data.enable_disable],
            "createdCell": function (td, cellData, rowData, row, col) {
                var icon = "<i class='fa fa-lg fa-check-circle-o text-success'></i>";
                
                if(rowData[data.enable_disable] != 1){
                     icon = "<i class='fa fa-lg fa-lock text-danger'></i>";
                }
                $(td).html(icon);     
            }                                        
        });                
    }

    if(data.table_uppercase){
        $(tableID).css("text-transform","uppercase");    
    }

    if(data.id){
        objParam.id = data.id;
    }

    if(data.sScrollY){
        objParam.sScrollY = data.sScrollY;
    }

    if(data.totalColumns){
        objParam.totalColumns = data.totalColumns;
    }

    //EXTERNAL ROW FUNCTION CALL
    if(data.externalRowFunction){ //THIS FUNCTION MUST BE DECLARED AS GLOBAL SCOPE
       //eval(data.externalRowFunction); //REPLACED EVAL WITH SECURE TECHNIQUE
       var fn = window[data.externalRowFunction];
       if (typeof fn === "function") fn(objParam);
    }

    //CHANGE LANGUAGE IF NOT ENGLISH
    if($("#custom_dash_language").val() != globalDtLanguage){
       objParam.language = {"url":"../common/languages/"+$("#custom_dash_language").val()+"/datatable.json"};
    }

    //console.log(JSON.stringify(objParam));

    var tableOptions = globalGenerateTableOptions(objParam);

    var name = objParam.id+"_datatable";
    globalDTObjectReference[name] = data.id+"_datatable";    

    globalDTObjectReference[name] = $(tableID).dataTable(tableOptions);
} 

function globalTextTruncate( n, useWordBoundary ){
    if (this.length <= n) { return this; }
    var subString = this.substr(0, n-1);
    return (useWordBoundary 
       ? subString.substr(0, subString.lastIndexOf(' ')) 
       : subString) + "...";
} 

function globalZIndexHighest(){
    var zIndexVal=0;
    $('*').each(function(){
        if(parseInt($(this).css('zIndex')) > zIndexVal) zIndexVal = $(this).css('zIndex');
    });
    return zIndexVal;    
} 

function globalGetParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}   

function globalHexToRgb(hex) {
    var bigint = parseInt(hex, 16);
    var r = (bigint >> 16) & 255;
    var g = (bigint >> 8) & 255;
    var b = bigint & 255;
    
    return r + "," + g + "," + b;
}

function globalLoadingMessage(id){
    $("#"+id).html("<div class='text-center'><label class='label custom-loading-label-dh'><img src='"+globalCustomImgPath+"logo/chart-loading-white.png'> "+$("#chart_loading").val()+" <i class='fa fa-gear fa-spin'></i></label></div>");
}

function globalNoDataMessage(id){
    $("#"+id).html("<div class='text-center'><label class='label custom-loading-label-dh faa-flash animated'><img src='"+globalCustomImgPath+"logo/chart-loading-white.png'> "+$("#chart_nodata").val()+" </label></div>");
}

function globalFailedMessage(id){
    $("#"+id).html("<div class='text-center'><label class='label custom-loading-label-dh faa-flash animated'><img src='"+globalCustomImgPath+"logo/chart-loading-white.png'> "+$("#chart_failed").val()+" </label></div>");
}

function globalGenerateChart(portletId,item,dataObj){
    //GET CHART OBJECT FROM STANDARD
    var chartClone = item.chartclone;
    var chartConfig = jQuery.extend(true, {}, amchartsObjects['options'][chartClone]);

    //MERGE WITH OBJECT FROM PORTLET
    var mergeChartConfig = _.merge({}, chartConfig, item);

    //ADDITIONAL PARAM
    mergeChartConfig['id'] = portletId;

    if(globalModalFullscreen){
        mergeChartConfig.divId = globalModalFullScreenAttr.divId;        
        mergeChartConfig.sourceId = item.sourceId;     
    }
    amchartsObjects[chartClone](mergeChartConfig);    
}

function globalChangeChart(portletId,changeChart,chartType,eventType){
    var portletBodyId = portletId+"_body";
    var chartClone = $("#"+portletBodyId).attr("data-chartconfig");
    var chartConfig = jQuery.extend(true, {}, amchartsObjects['options'][chartClone]);

    var portletChartAttr = JSON.parse($("#"+portletBodyId).attr("data-chartattr"));

    //MERGE WITH OBJECT FROM PORTLET
    var mergeChartConfig = _.merge({}, chartConfig, portletChartAttr);
    var portletChartClick = JSON.parse($("#"+portletBodyId).attr("data-chartclick"));
    var portletQueryString = JSON.parse($("#"+portletBodyId).attr("data-querystring"));

    if(changeChart){
        $("#"+portletBodyId).attr("data-graph-type",chartType);
    }

    mergeChartConfig['querystring'] = portletQueryString;
    mergeChartConfig['eventType'] = eventType;
    mergeChartConfig['id'] = portletId;
    mergeChartConfig['divId'] = portletBodyId;
    mergeChartConfig['chartType'] = chartType;
    mergeChartConfig['changeChart'] = changeChart;
    mergeChartConfig['chartclick'] = portletChartClick;
                
    amchartsObjects[chartClone](mergeChartConfig);                                      
}

function globalExpandCollapsePortlet(portletId){
    if(document.getElementById(portletId)){
        if($("#"+portletId+" .collapse").attr("data-expand") == 0){  
            $("#"+portletId+" .collapse").click();
            $('html, body').animate({scrollTop:50},'10');
        }             
    }
}

function globalCopyClipboard(id) {
    var elm = document.getElementById(id);
    // for Internet Explorer
  
    if(document.body.createTextRange) {
      var range = document.body.createTextRange();
      range.moveToElementText(elm);
      range.select();
      document.execCommand("Copy");
      toastr.success($("#toastr_generic_success_desc").val(),$("#toastr_generic_success_title").val());
    }
    else if(window.getSelection) {
      // other browsers
  
      var selection = window.getSelection();
      var range = document.createRange();
      range.selectNodeContents(elm);
      selection.removeAllRanges();
      selection.addRange(range);
      document.execCommand("Copy");
      toastr.success($("#toastr_generic_success_desc").val(),$("#toastr_generic_success_title").val());
    }
}

function globalCheckLatLon(lat, lon){
    var lat_check = /^(-?[1-8]?\d(?:\.\d{1,18})?|90(?:\.0{1,18})?)$/;
    var lon_check = /^(-?(?:1[0-7]|[1-9])?\d(?:\.\d{1,18})?|180(?:\.0{1,18})?)$/;
    var validLat = lat_check.test(lat);
    var validLon = lon_check.test(lon);
    if(validLat && validLon) {
        return true;
    } 
    else {
        return false;
    }
}

//START INPUT VALIDATION
var globalSpecialCharsDefault = "^[a-zA-Z0-9-_\b ]+$";
var globalCurrencyCharsDefault = "^[0-9.\b ]+$";
var globalIntegerCharsDefault = "^[0-9\b ]+$";
var globalNewIcDefault = "^[0-9\b]+$";
var globalOldIcDefault = "^[a-zA-Z0-9\b]+$";
var globalIntegerCharsNoPaste = "^[0-9\b]+$";

//BIND AT KEYPRESS
$(document).on('keypress', '.custom-currencychars', function(event){
    var regex = new RegExp(globalCurrencyCharsDefault);
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});  

$(document).on('keypress', '.custom-nospecialchars', function(event){
    var regex = new RegExp(globalSpecialCharsDefault);
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});

$(document).on('keypress', '.custom-integerchars', function(event){
    var regex = new RegExp(globalIntegerCharsDefault);
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});

$(document).on('keypress', '.custom-newicchars', function(event){
    var regex = new RegExp(globalNewIcDefault);
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});

$(document).on('keypress', '.custom-oldicchars', function(event){
    var regex = new RegExp(globalOldIcDefault);
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});

$(document).on('keypress', '.custom-integerchars-nopaste', function(event){
    var regex = new RegExp(globalIntegerCharsNoPaste);
    var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
    if (!regex.test(key)) {
       event.preventDefault();
       return false;
    }
});
//BIND AT KEYPRESS

//BIND AT BLUR
$(document).on('blur', '.custom-currencychars', function(event){
    if(!parseFloat($(this).val())){
        $(this).val("");
    }
    else{
        var floatNum = parseFloat($(this).val());
        floatNum = floatNum.toFixed(2);
        $(this).val(floatNum);
    }
});

$(document).on('blur', '.custom-integerchars', function(event){
    if(!parseInt($(this).val())){
        $(this).val("");
    }
    else{
        $(this).val(parseInt($(this).val()));
    }
});

//BIND AT PASTE
$(document).on('paste', '.custom-currencychars', function(event){
    return false;
});

$(document).on('paste', '.custom-nospecialchars', function(event){
    return false;
});

$(document).on('paste', '.custom-integerchars', function(event){
    return false;
});

$(document).on('paste', '.custom-integerchars-nopaste', function(event){
    return false;
});
//END INPUT VALIDATION

var globalIntervalIds = [];
var globalIntervalTimers = {};

//INTERVAL GENERATOR
function globalIntervalGenerator(type,id,interval,stream=false,catlimit=0){
    //2 INSTANCES OF CHART CANNOT BE REFRESHED AT THE SAME TIME
    $("#"+id).find(".custom-fullscreen-ind-button").addClass("hidden");

    if(type == "chart"){
        var dataStream = stream;
        var dataStreamLimit = catlimit;

        if(dataStream){
            $("#"+id).find(".toggle-filter-visible").addClass("hidden");
        }

        var portletId = id;
        var portletBodyId = portletId+"_body";

        for(var i = 0; i < AmCharts.charts.length; i++) {
            if(AmCharts.charts[i].id == id){                
                var chartID = i;
                if(globalIntervalIds.indexOf(id) < 0){
                    globalIntervalIds.push(id);
                    $("#"+id+" .custom-caption-interval-pending").removeClass("hidden");
                    $("#"+id+" .custom-caption-interval-pending").attr("data-original-title","PAUSE / RESUME "+interval/1000+"s");

                    globalIntervalTimers[id] = setInterval(function() {  
                        if($("#"+portletId).find(".custom-caption-interval-pending").hasClass("btn-default")){
                            $("#"+id+" .custom-caption-interval-pending").addClass("hidden");
                            $("#"+id+" .custom-caption-interval-exec").removeClass("hidden");

                            $.post( "backend/ajax_call.php", AmCharts.charts[chartID].querystring)
                            .done(function( data ) {
                                data = JSON.parse(data);

                                //DYNAMIC SHIFT AND PUSH
                                var targetLength = data[0].dataProvider.length;
                                for(var i = 0; i < targetLength; i++){
                                    data[0].dataProvider[i].color = AmCharts.charts[chartID].dataProvider[i].color;
                                    AmCharts.charts[chartID].dataProvider.push(data[0].dataProvider[i]);
                                }

                                if(dataStream){
                                    if(AmCharts.charts[chartID].dataProvider.length > dataStreamLimit){
                                        AmCharts.charts[chartID].dataProvider.shift();
                                    }
                                }
                                else{
                                    AmCharts.charts[chartID].dataProvider.splice(0,targetLength);                            
                                }
                                

                                var chart = AmCharts.charts[chartID];
                                chart.validateData();

                                $("#"+id+" .custom-caption-interval-pending").removeClass("hidden");
                                $("#"+id+" .custom-caption-interval-exec").addClass("hidden");
                            });
                        }                            
                    }, interval);            
                }                
            }
        }
    }
    else if(type == "grid"){
        if(globalIntervalIds.indexOf(id) < 0){
            globalIntervalIds.push(id);
            $("#"+id+" .custom-caption-interval-pending").removeClass("hidden");
            $("#"+id+" .custom-caption-interval-pending").attr("data-original-title","PAUSE / RESUME "+interval/1000+"s");

            var portletId = id;
            var portletBodyId = portletId+"_body";
            
            globalIntervalTimers[id] = setInterval(function() {
                if($("#"+portletId).find(".custom-caption-interval-pending").hasClass("btn-default")){  
                    $("#"+id+" .custom-caption-interval-pending").addClass("hidden");
                    $("#"+id+" .custom-caption-interval-exec").removeClass("hidden");

                    var gridConfig = JSON.parse($("#"+portletBodyId).attr("data-grid"));
                    var params = {
                        method: gridConfig.method,
                        portletId: portletId,
                        calltype: gridConfig.calltype
                    };                                
                    $.get("backend/ajax_call.php?"+$.param(params))
                    .done(function( data ) {       
                        var JSONdata = JSON.parse(data);
                        for (var key in JSONdata) {   
                            var value = JSONdata[key]; 
                            $("#"+portletBodyId).find(".generator-grid-"+key).attr("data-value",value);
                            $("#"+portletBodyId).find(".generator-grid-"+key).text(value);
                        } 
                        $("#"+id+" .custom-caption-interval-pending").removeClass("hidden");
                        $("#"+id+" .custom-caption-interval-exec").addClass("hidden");
                    });
                }                                
            }, interval);            
        }        
    } 
    else if(type == "mapgen"){
        if(globalIntervalIds.indexOf(id) < 0){
            globalIntervalIds.push(id);
            $("#"+id+" .custom-caption-interval-pending").removeClass("hidden");
            $("#"+id+" .custom-caption-interval-pending").attr("data-original-title","PAUSE / RESUME "+interval/1000+"s");

            var portletId = id;
            var portletBodyId = portletId+"_body";
            
            globalIntervalTimers[id] = setInterval(function() {  
                if($("#"+portletId).find(".custom-caption-interval-pending").hasClass("btn-default")){
                    $("#"+id+" .custom-caption-interval-pending").addClass("hidden");
                    $("#"+id+" .custom-caption-interval-exec").removeClass("hidden");

                    var extraAttr = JSON.parse($("#"+portletBodyId).attr("data-mapgen"));
                    var mapConfig = {
                        id: portletId,
                        mapId: portletBodyId,
                        mapContentOwner: globalMapContentOwner,  
                        mapContentOwnerURL: globalMapContentOwnerURL,
                        countryCode: $("#"+portletBodyId).attr("data-countrycode"),
                        extraAttr: extraAttr,
                        interval: true
                    };
                    globalMapObjectReference[portletId+"_mapgen"].remove();
                    mapgenObjects.map_main(mapConfig);  
                }
            }, interval);            
        }        
    } 
    else if(type == "datatables"){
        if(globalIntervalIds.indexOf(id) < 0){
            globalIntervalIds.push(id);
            $("#"+id+" .custom-caption-interval-pending").removeClass("hidden");
            $("#"+id+" .custom-caption-interval-pending").attr("data-original-title","PAUSE / RESUME "+interval/1000+"s");

            var portletId = id;
            var portletBodyId = portletId+"_body";
            
            globalIntervalTimers[id] = setInterval(function() {
                if($("#"+portletId).find(".custom-caption-interval-pending").hasClass("btn-default")){
                    $("#"+id+" .custom-caption-interval-pending").addClass("hidden");
                    $("#"+id+" .custom-caption-interval-exec").removeClass("hidden");
                    
                    globalDTObjectReference[portletId+"_datatable"].api().ajax.reload();
    
                    $("#"+id+" .custom-caption-interval-pending").removeClass("hidden");
                    $("#"+id+" .custom-caption-interval-exec").addClass("hidden");    
                }  
            }, interval);            
        }        
    }          
}

function globalCloseChildWindows(url){
    for (var i=0; i<globalOpenedWindows.length; i++) {
        if (!globalOpenedWindows[i].closed){
            globalOpenedWindows[i].close();
        }
    }
    window.location.replace(url);
}

function globalModalFullCenter(modalId){
    $(modalId).css('margin-top', ($(modalId).outerHeight() / 2) * -1).css('margin-left', ($(modalId).outerWidth() / 2) * -1);  									
}

function globalReloadLocation(){
    location.reload();
}

function globalFormatBytes(bytes) {
    if(bytes < 1024) return bytes + " Bytes";
    else if(bytes < 1048576) return(bytes / 1024).toFixed(2) + " Kb";
    else if(bytes < 1073741824) return(bytes / 1048576).toFixed(2) + " Mb";
    else return(bytes / 1073741824).toFixed(2) + " Gb";
}

function globalResetFormValidator(arrIds){
    for(var i=0; i<arrIds.length; i++){
        var validator = $(arrIds[i]).validate();
        validator.resetForm();
    }
}

//FOR JKSM ONLY - NOT USING AUTO GENERATED DT OPTIONS
//DATATABLE OPTIONS
var spkmsDT_buttons = [
    {
        extend: 'colvis',
        text: 'Kolum',
        className: 'custom-dt-btn-info',
        columns: ':not(.noVis)'
    },
    {
        extend: 'copy',
        text: 'Salin',
        className: 'custom-dt-btn-info',
        exportOptions: {
            columns: ':visible'
        }                
    },
    {
        extend: 'csv',
        className: 'custom-dt-btn-info',
        exportOptions: {
            columns: ':visible'
        }                
    },
    {
        extend: 'excel',
        className: 'custom-dt-btn-info',
        exportOptions: {
            columns: ':visible'
        }                
    },
    {
        extend: 'pdf',
        className: 'custom-dt-btn-info',
        orientation: 'landscape',
        exportOptions: {
            columns: ':visible',
            stripNewlines: false
        }                
    },
    {
        extend: 'print',
        text: 'Cetak',
        className: 'custom-dt-btn-info',
        exportOptions: {
            columns: ':visible',
            stripNewlines: false
        }                
    }
];

var spkmsDT_aLengthMenu = [[10, 25, 50, 100, -1], [10, 25, 50, 100, $("#datatables_all").val()]];
var spkmsDT_dom = "<'row'<'col-sm-6'Bl><'col-sm-6'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'p>>";

var spkmsDT_language = {
    "url": ""
};
if($("#custom_dash_language").val() == "bm"){
    spkmsDT_language.url = "../common/languages/bm/datatable.json";
}
//FOR JKSM ONLY - NOT USING AUTO GENERATED DT OPTIONS
//DATATABLE OPTIONS

//START GLOBAL GPKI

//START PENJANAAN GPKI
$(".custom-child-link-gpki").on("click", function(){
    var url = $(this).attr("data-url");
    var name = $(this).attr("data-name");

    $("#esv3_gpki_return_statuscode").val("");
    $("#esv3_gpki_return_statusmsg").val("");
    $("#esv3_gpki_return_certtype").val("");

    $.blockUI({ 
        message: "<img src='"+globalCustomImgPath+"logo/loading-logo.png'><br><span class='faa-flash animated'><i class='fa fa-key'></i> SEDANG MENGESAN MAKLUMAT GPKI. SILA TUNGGU...</span>", 
        baseZ: 12000,
        css: globalBlockUICSS
    });

    var data = [];
    data.push({name:'user_id',value: $("#custom_dash_user_id").val()});
    data.push({name:'method',value: "get_details"});
    $.get("../common/security/gpki/ws_gpki.php",data).done(function(data) {
        var JSONdata = JSON.parse(data);
        var CERTIFICATE_TYPE = JSONdata.cert_type;
        var pin = JSONdata.cert_pin;
        var id = JSONdata.cert_id;

        $("#esv3_gpki_return_certtype").val(CERTIFICATE_TYPE);

        if(CERTIFICATE_TYPE === "roaming"){
            verify_id(CERTIFICATE_TYPE, id);
        }
        else if(CERTIFICATE_TYPE === "softcert"){
            sign(CERTIFICATE_TYPE ,id,pin, encodeURIComponent(Gpki.hash('')),true);
        }        
        else{
            if($("#custom_gpki_production").val() == "0"){
                $("#esv3_gpki_return_statuscode").val("0");
            }
            else{
                sign(CERTIFICATE_TYPE ,id,pin, encodeURIComponent(Gpki.hash('')),true);
            }            
        }			

        var WAIT_certificate_result = setInterval(GET_certificate_result,500);

        function GET_certificate_result(){
            if($("#esv3_gpki_return_statuscode").val() != ""){
                $.unblockUI();   
                clearInterval(WAIT_certificate_result);	
                if(($("#esv3_gpki_return_statuscode").val() == "0" && CERTIFICATE_TYPE !== "roaming") 
                    || ($("#esv3_gpki_return_statuscode").val() == "200" && CERTIFICATE_TYPE === "roaming")
                    || ($("#esv3_gpki_return_statuscode").val() == "0" && CERTIFICATE_TYPE === "roaming")){	  
                        if(name == "temporary"){
                            swal({
                                type: 'info',
                                title: type,
                                text: 'Modul Ini Masih Dalam Peringkat Development'
                            }); 
                        }	
                        else{
                            if(name == "_blank"){
                                globalOpenedWindows[globalOpenedWindows.length] = window.open(url,name);
                            }
                            else{
                                globalCloseChildWindows(url);
                            }                     						        
                        }	                    
                }
                else{
                    swal(
                        'STATUS GPKI - GAGAL',
                        'Error Code: '+$("#esv3_gpki_return_statuscode").val()+'<br>'+$("#esv3_gpki_return_statusmsg").val(),
                        'error'
                    );				
                }									
            }
        }			
    });    
});
//END PENJANAAN GPKI

function callback_verify_id(msg){
    $.unblockUI();
    if (swal.isVisible()) {
        swal.close();
    }    
    var obj = jQuery.parseJSON(msg);
    var status_code = obj.status_code;
    var status_message = obj.status_message;
    var image = obj.image;
    question = obj.question;
    certID = obj.cert_id;

    if(status_code == "0") {
        var image_url = "<img border=\"0\" src=\"http://127.0.0.1:8084/resource?get=" + image +
            "\"  width=\"150\">";

        $("#gpki_api_modal #modal_gpki_userimage").html(image_url);
        $("#gpki_api_modal #question").append(question);
        $("#gpki_api_modal #certID").val(certID);
        $('#gpki_api_modal').modal({backdrop: 'static', keyboard: false});	
    }
    else{
        //ADDED BY REDZUAN - FLAGS
        $("#esv3_gpki_return_statuscode").val(status_code);
        $("#esv3_gpki_return_statusmsg").val(status_message);	     

        if(status_code == "404"){
        }
        else if(status_code== "20"){
            $("#gpki_api_modal #certID").val(certID);
        }
        else{
            $('#gpki_api_modal').modal('hide');
        }
    }
}

$("#gpki_api_modal #btn_hantar").on("click",function(e){
    if(($("#gpki_api_modal #answer").val() == "") || ($("#gpki_api_modal #pin").val() == "")){
        $("#gpki_api_modal #empty_field").show();
    } 
    else{
        $("#gpki_api_modal #empty_field").hide();

        var certID = $("#gpki_api_modal #certID").val();
        var pin = $("#gpki_api_modal #pin").val();
        var type = "roaming";
        var answer = $("#gpki_api_modal #answer").val();
        var certID = $("#gpki_api_modal #certID").val();
        var question = $("#gpki_api_modal #question").val();

        verify_question(type, certID, pin, question, answer, 10);
    }
});

function processVerifyQuestionResult(msg){
    var obj = jQuery.parseJSON(msg);
    var status_code = obj.status_code;
    var status_message = obj.status_message;

    if(status_code == "0"){
        var data = [];
        data.push({name:'user_id',value: $("#custom_dash_user_id").val()});
        data.push({name:'method',value: "get_details"});
        $.get("../common/security/gpki/ws_gpki.php",data).done(function(data) {
            var JSONdata = JSON.parse(data);
            var CERTIFICATE_TYPE = JSONdata.cert_type;
            var pin = JSONdata.cert_pin;
            var id = JSONdata.cert_id;

            sign(CERTIFICATE_TYPE ,id,pin, encodeURIComponent(Gpki.hash('')),true);
        });
        $('#gpki_api_modal').modal('hide');
    }
    else{
        //ADDED BY REDZUAN - FLAGS
        $("#esv3_gpki_return_statuscode").val(status_code);
        $("#esv3_gpki_return_statusmsg").val(status_message);	     
        $('#gpki_api_modal').modal('hide');
        //if(status_code == "10"){
            //$('#gpki_api_modal').modal('hide');
    
        //}
        //else{
        //    $('#gpki_api_modal').modal('hide');
        //}
    }
}

$("#gpki_api_modal #btn_batal").on("click",function(e){
    $('#gpki_api_modal').modal('hide');
});

var data = [];
data.push({name:'user_id',value: $("#custom_dash_user_id").val()});
data.push({name:'method',value: 'get_user_integration_status'});

if(url_backend){
	var url_pengurusan = url_backend + "/spkms/backend/ws_pengurusan_kes.php";
} else {
	var url_pengurusan =  "../spkms/backend/ws_pengurusan_kes.php";
}
$.get(url_pengurusan,data).done(function(data){
    var JSONdata = JSON.parse(data);
    $("#custom_gpki_production").val(JSONdata.user_int_gpki);
    $("#custom_gpki_production").val(JSONdata.user_int_gpki);
});
  
//END GLOBAL GPKI

//JSON PARSER
function globalJSONLint(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}