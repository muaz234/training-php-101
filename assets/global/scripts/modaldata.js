var modalDataObjects = {
	options : {  
        googlemap_split: {
            "target":"modal", 
            "link_id":"generic_gmsplit_list_link", 
            "modal_id":"generic_gmsplit_list_modal", 
            "type":"googlemap_split", 
            "target_id":"generic_gmsplit_list_body", 
            "title_text":"LOCATION", 
            "title_icon":"fa fa-map-marker", 
            "lat": 0, 
            "lng":0, 
            "split_body_id":"generic_gmsplit_list_body_map", 
            "split_pano_id":"generic_gmsplit_list_body_pano",
            "streetview":0
        },                                              	            
	   
        generic_datatables: {
            "target":"modal",
            "link_id":"generic_dt_list_link",
            "modal_id":"generic_dt_list_modal",
            "type":"generic_datatables",
            "target_id":"generic_dt_list_table",
            "title_text":"LIST",
            "title_icon":"fa fa-table",
            "method":"get_list_maplocations",
            //"extraquery_link_id":[],
            "extraquery_title": 0,
            "calltype":"datatables",
            "enable_disable": -1,
            "order":[0,"asc"],
            "visibility":[],
            "colvis":[],
            "table_th":""
            //"table_uppercase":"true"                                                   	                                                
        },

        feedback_modal: {
            "target":"modal",
            "link_id":"support_div_list_link",
            "modal_id":"support_div_list_modal",
            "title_icon":"fa fa-envelope"
        },

        apikey_modal: {
            "target":"modal",
            "link_id":"apikey_div_list_link",
            "modal_id":"apikey_div_list_modal",
            "title_icon":"fa fa-key"
        },        

        custom_modal: {
            "target":"modal",
            "type":"custom_modal",
            "title_text":"",
            "title_icon":"fa fa-table"
        }
	}	        
}