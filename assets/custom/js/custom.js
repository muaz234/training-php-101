function resetForm(formId, className) {
  let quillCounter = 0;

  $.each($('#' + formId + ' ' + '.' + className), function () {
    if ($(this).is('select')) {
      $('#' + this.id).val('-1').trigger('change');
    } else if ($(this).is(':text') || $(this).is("textarea")) {
      $('#' + this.id).val('');
    } else if ($(this).hasClass('dropzone')) {
      let myDropzone = Dropzone.forElement("#" + this.id);
      myDropzone.removeAllFiles();
    } else if ($(this).hasClass('quill')) {
      quillCounter++;
    }

  })

  if (quillCounter > 0) {
    $('.ql-editor').html('');
  }
}

function disableForm(formId, className) {
  $.each($('#' + formId + ' ' + '.' + className), function () {

    $('#' + this.id).prop("disabled", true);
  });
}

//Session timeout https://stackoverflow.com/questions/667555/how-to-detect-idle-time-in-javascript-elegantly
function sessionTimeout() {
  var t;
  window.onload = resetTimer;
  window.onmousemove = resetTimer;
  window.onmousedown = resetTimer;  // catches touchscreen presses as well
  window.ontouchstart = resetTimer; // catches touchscreen swipes as well
  window.onclick = resetTimer;      // catches touchpad clicks as well
  window.onkeypress = resetTimer;
  window.addEventListener('scroll', resetTimer, true); // improved; see comments

  function timeout() {
    // your function for too long inactivity goes here
    window.location.href = "https://login.php?status=session&lang=bm";
  }

  function resetTimer() {
    clearTimeout(t);
    t = setTimeout(timeout, 300000);  // time is in milliseconds = 5 minutes
  }
}

function swala(status) {
  if (status == "error") {
    swal("STATUS GAGAL", "Sila Hubungi Administrator", "error");
  } else if (status == "success") {
    swal("STATUS BERJAYA", "Activiti Berjaya Dilakukan", "error");
  }

}

function toastra(status) {
  if (status == "error") {
    toastr.error("Sila Hubungi Pentadbir Sistem", "STATUS GAGAL");
  } else if (status == "success") {
    toastr.success("STATUS BERJAYA", "Activiti Berjaya Dilakukan");
  }
}

function checkTableHeader(tableId) {

  if ($("#" + tableId + " >tbody > tr").length == 0) {
    $("#" + tableId + " thead > tr").css('visibility', 'hidden');
  } else {
    $("#" + tableId + " thead > tr").css('visibility', 'visible');
  }
}

function listDocumentKes(pendaftaran_id, folder, tableId, final_path, deleteColumn = true) {
  var data = [];
  data.push({name: 'pendaftaran_id', value: pendaftaran_id});
  data.push({name: 'folder', value: folder});
  data.push({name: 'method', value: "get_dokumen_kes"});
  data.push({name: 'final_path', value: final_path})

  $.get("backend/ws_dokumen.php", data).done(function (data) {
    var JSONdata = data;
    var tableData = "";

    if (JSONdata.length > 0) {
      for (var i = 0; i < JSONdata.length; i++) {
        if (deleteColumn) {
          var deleteCol = "<td class='modal_kes_hide_non_pendaftar'><a folder='" + folder + "' pendaftaran_id='" + pendaftaran_id + "' filename='" + JSONdata[i].filename + "' class='btn-delete-dokumen-kes btn btn-outline btn-sm red'><i class='fa fa-trash'></i></a></td>";
        } else {
          var deleteCol = "";
        }

        tableData += "<tr><td>" + JSONdata[i].counter + ".</td><td><a target=_blank href='" + JSONdata[i].filepath + "'>" + JSONdata[i].filename + "</a></td><td>" + formatBytes(JSONdata[i].filesize) + "</td><td>" + JSONdata[i].filetype + "</td><td>" + JSONdata[i].filetime + "</td>" + deleteCol + "</tr>";
      }
    }
    $("#" + tableId + " tbody").html(tableData);
    checkTableHeader(tableId);


  });
}

function formatBytes(bytes) {
  if (bytes < 1024) return bytes + " Bytes";
  else if (bytes < 1048576) return (bytes / 1024).toFixed(2) + " Kb";
  else if (bytes < 1073741824) return (bytes / 1048576).toFixed(2) + " Mb";
  else return (bytes / 1073741824).toFixed(2) + " Gb";
};

//DATATABLE OPTIONS
var sppsDT_buttons = [
  {
    extend: 'colvis',
    text: 'Columns',
    className: 'custom-dt-btn-info',
    columns: ':not(.noVis)'
  },
  {
    extend: 'copy',
    className: 'custom-dt-btn-info',
    exportOptions: {
      columns: ':visible'
    }
  },
  {
    extend: 'csv',
    className: 'custom-dt-btn-info',
    exportOptions: {
      columns: ':visible'
    }
  },
  {
    extend: 'excel',
    className: 'custom-dt-btn-info',
    exportOptions: {
      columns: ':visible'
    }
  },
  {
    extend: 'pdf',
    className: 'custom-dt-btn-info',
    orientation: 'landscape',
    exportOptions: {
      columns: ':visible'
    }
  },
  {
    extend: 'print',
    className: 'custom-dt-btn-info',
    exportOptions: {
      columns: ':visible'
    }
  }
];

var sppsDT_aLengthMenu = [[10, 25, 50, 100, -1], [10, 25, 50, 100, $("#datatables_all").val()]];
var sppsDT_dom = "<'row'<'col-sm-9'Bl><'col-sm-3'f>>" + "<'row'<'col-sm-12'tr>>" + "<'row'<'col-sm-5'i><'col-sm-7'p>>";

var sppsDT_language = {
  "url": ""
};
if ($("#custom_dash_language").val() == "bm") {
  sppsDT_language.url = "../common/languages/bm/datatable.json";
}
