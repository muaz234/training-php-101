<?php
/*error_reporting(-1); ini_set('display_errors', 'On'); mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);*/
//MANDATORY FOR GLOBAL INIT - DO NOT DELETE!!!
//include_once("backend/ws_url.php");

?>

<?php
//PAGE SELECTOR
if (!isset($_GET['page'])) {
    include("index_links/dashboard/fragments/local_content.php");
} else {
    if (isset($_GET['page']) && $_GET['page'] == 'dashboard') {
        include("index_links/dashboard/fragments/local_content.php");
    }
    else if (isset($_GET['page']) && $_GET['page'] == 'employees') {
        include("index_links/employees/fragments/local_content.php");
    }

}
?>

<!DOCTYPE html>
<!--[if IE 8]>
<html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8"/>
    <title>Training</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport"/>
    <meta content="E-Syariah 3.0" name="MTSSB"/>
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/clockface/css/clockface.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal-bs3patch.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/bootstrap-modal/css/bootstrap-modal.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <!--        <link href="//cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
    -->        <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css"/>
    <link href="assets/global/css/plugins.min.css" rel="stylesheet" type="text/css"/>
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css"
          id="style_color"/>
    <!-- END THEME LAYOUT STYLES -->
    <!-- BEGIN DATATABLES -->
    <link href="assets/global/plugins/datatables/DataTables-1.10.16/css/dataTables.bootstrap.min.css"
          rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/datatables/Buttons-1.4.2/css/buttons.bootstrap.css" rel="stylesheet"
          type="text/css"/>
    <link href="assets/global/plugins/datatables/Select-1.3.1/css/select.dataTables.min.css"
          rel="stylesheet" type="text/css"/>
    <!-- END DATATABLES -->
    <!-- BEGIN BOOTSTRAP SELECT -->
    <link href="assets/global/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet"
          type="text/css"/>
    <!-- END BOOTSTRAP SELECT -->
    <!-- BEGIN QUILL -->
    <link href="assets/global/plugins/quill/quill.snow.css" rel="stylesheet" type="text/css"/>
    <!-- END QUILL -->
    <!-- BEGIN CUSTOM LAYOUT -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:200,400,700" rel="stylesheet"/>
    <!--    <link href="../common/assets/global/plugins/amcharts/amcharts/plugins/export/export.css" rel="stylesheet" type="text/css"/>
        <link href="../common/assets/global/plugins/mapping/leaflet-120/leaflet.css" rel="stylesheet" type="text/css"/>
        <link href="../common/assets/global/plugins/mapping/awesome-markers/leaflet_awesome_markers.css" rel="stylesheet" type="text/css"/>
        <link href="../common/assets/global/plugins/mapping/css/main.css" rel="stylesheet" type="text/css"/>
        <link href="../common/assets/global/plugins/mapping/css/layout.css" rel="stylesheet" type="text/css"/>
        <link href="../common/assets/global/plugins/mapping/locate/css/L.Control.Locate.min.css" rel="stylesheet" type="text/css"/>
        <link href="../common/assets/global/plugins/mapping/polyline-measure/Leaflet.PolylineMeasure.css" rel="stylesheet" type="text/css"/>-->
    <!--    <link href="../common/assets/global/plugins/toastr/toastr.min.css" rel="stylesheet" type="text/css"/>-->
    <link href="assets/global/css/general.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/css/responsive.css" rel="stylesheet" type="text/css"/>
    <!--    <link href="../common/assets/global/css/modules.css" rel="stylesheet" type="text/css"/>-->
    <link href="assets/global/css/modules.css" rel="stylesheet" type="text/css" />

    <link href="assets/global/themes/default.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css"/>
    <link href="assets/global/plugins/uploader/dist/css/jquery.dm-uploader.min.css" rel="stylesheet"
          type="text/css" />
    <link href="assets/custom/css/custom.css" rel="stylesheet" type="text/css"/>
    <!-- END CUSTOM LAYOUT -->

    <style>
        .select2-container--open {
            z-index: 9999999
        }

        .select2-results__group {
            font-size: 14px !important;
        }

        .select2-results__option {
            font-size: 12px !important;
        }
    </style>

    <?php echo $style; ?>


    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="assets/global/img/logo/favicon.png"/>
</head>
<!-- END HEAD -->

<body class="page-header-fixed page-sidebar-hide page-sidebar-closed">
<?php
//include_once("includes/preloader.php");
?>
<div class="page-wrapper">
    <!-- BEGIN HEADER -->
    <div class="page-header navbar navbar-fixed-top">
        <!-- BEGIN HEADER INNER -->
        <div class="page-header-inner ">
            <!-- BEGIN LOGO -->
            <div class="page-logo">
                <a href="#">
                    <img src="assets/global/img/logo/header-logo.png" alt="logo" class="logo-default"/> </a>
                <div class="menu-toggler sidebar-toggler">
                    <span></span>
                </div>
            </div>
            <!-- END LOGO -->
            <!-- BEGIN TOP NAVIGATION MENU -->
            <div class="top-menu">
                <ul class="nav navbar-nav pull-right">
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after "dropdown-extended" to change the dropdown styte -->
                    <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                    <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended dropdown-tasks hidden">
                        <?php //$data_theme = $init->get_data('get_theme_data'); ?>
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true" aria-expanded="false">
                            <i class="fa fa-image"></i>
                            <span class="badge hidden"> <?php //echo $data_theme[0]['theme_total']; ?> </span>
                        </a>
                        <ul class="dropdown-menu extended tasks">
                            <li class="external">
                                <h3>Themes</h3>
                            </li>
                            <li>
                                <div style="position: relative;width: auto;">
                                    <ul class="dropdown-menu-list scroller" style="width: auto;"
                                        data-handle-color="#637283" data-initialized="1">
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                           data-close-others="true">
                            <img alt="" class="img-circle" src="assets/global/img/users/default.png"/>
                            <span class="username username-hide-on-mobile"> Training User </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li class="custom-child-link"
                                data-type="landing" data-name="_self">
                                <a class="custom-logout-link" href="./index.php" >
                                    <i class="icon-list"></i> <span>Page Utama</span>
                                </a>
                            </li>
                            <li class="custom-event-click">
                                <a href="./index.php"><i class="icon-envelope"></i> <span>Maklumbalas</span></a>
                            </li>
                            <li class="custom-child-link"

                                data-type="logout" data-name="_self">
                                <a class="custom-logout-link" href="./index.php">
                                    <i class="icon-power"></i> <span>Log Keluar</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                </ul>
            </div>
            <!-- END TOP NAVIGATION MENU -->
        </div>
        <!-- END HEADER INNER -->
        <!-- START STATE FLAG HEADER -->
        <!-- END STATE FLAG HEADER -->
    </div>
    <!-- END HEADER -->
    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"></div>
    <!-- END HEADER & CONTENT DIVIDER -->
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN CONTENT -->
        <!-- BEGIN SIDEBAR -->
        <?php include("includes/sidebar.php"); ?>
        <!-- END SIDEBAR -->

        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content">
                <?php include("includes/page_header.php");
                ?>
                <?php echo $content ?>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
    <?php include("includes/page_footer.php"); ?>
    <!-- BEGIN LOAD ALL COMMON MODALS HERE -->
    <?php
    $dir = "../common/modals/";
    if (is_dir($dir)) {
        $arr_files = scandir($dir);
        foreach ($arr_files as $filename) {
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if ($ext === "php") {
                include($dir . $filename);
            }
        }
    }
    ?>
    <!-- END LOAD ALL RELEVANT MODALS HERE -->
    <!-- BEGIN LOAD ALL SPECIFIC MODALS HERE -->
    <?php
    $dir = "modals/";
    if (is_dir($dir)) {
        $arr_files = scandir($dir);
        foreach ($arr_files as $filename) {
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if ($ext === "php") {
                include($dir . $filename);
            }
        }
    }
    ?>
    <!-- END LOAD ALL SPECIFIC MODALS HERE -->
</div>
<!--[if lt IE 9]>
<script src="assets/global/plugins/respond.min.js"></script>
<script src="assets/global/plugins/excanvas.min.js"></script>
<script src="assets/global/plugins/ie8.fix.min.js"></script>
<![endif]-->
<!-- BEGIN CORE PLUGINS -->
<script src="assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<!--<script src="../common/assets/global/plugins/jquery-3.4.1.js" type="text/javascript"></script>-->
<script src="assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/clockface/js/clockface.js" type="text/javascript"></script>
<script src="assets/global/plugins/counterup/jquery.waypoints.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/counterup/jquery.counterup.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/fullcalendar/fullcalendar.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/fullcalendar/locale-all.js" type="text/javascript"></script>
<script src="assets/global/plugins/horizontal-timeline/horizontal-timeline.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/sweetalert/sweetalert2.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/jquery-validation/js/additional-methods.min.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js"
        type="text/javascript"></script>
<!--<script src="assets/pages/scripts/form-wizard.js" type="text/javascript"></script> not used in this script   -->
<script src="assets/custom/plugins/jquery-repeater-1.2.1/jquery.repeater-1.2.1.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-modal/js/bootstrap-modal.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-select2.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/lodash/lodash.full.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<!-- BEGIN DATATABLES PLUGINS -->
<script type="text/javascript"
        src="assets/global/plugins/datatables/DataTables-1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
        src="assets/global/plugins/datatables/DataTables-1.10.16/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript"
        src="assets/global/plugins/datatables/Buttons-1.4.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
        src="assets/global/plugins/datatables/Buttons-1.4.2/js/buttons.bootstrap.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/JSZip-2.5.0/jszip.min.js"></script>
<!--<script type="text/javascript" src="../common/assets/global/plugins/datatables/pdfmake-0.1.32/pdfmake.min.js"></script>-->
<script type="text/javascript" src="assets/global/plugins/datatables/pdfmake-0.1.32/vfs_fonts.js"></script>
<script type="text/javascript"
        src="assets/global/plugins/datatables/Buttons-1.4.2/js/buttons.html5.min.js"></script>
<script type="text/javascript"
        src="assets/global/plugins/datatables/Buttons-1.4.2/js/buttons.print.min.js"></script>
<script type="text/javascript"
        src="assets/global/plugins/datatables/Buttons-1.4.2/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="assets/global/plugins/datatables/Moment/datatables.moment.js"></script>
<!-- END DATATABLES PLUGINS -->
<!-- BEGIN BOOTSTRAP SELECT -->
<!--<script type="text/javascript" src="../common/assets/global/plugins/bootstrap-select/bootstrap-select.min.js"></script>-->
<!-- END BOOTSTRAP SELECT -->
<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<!-- BEGIN CUSTOM PLUGINS -->
<script src="https://maps.google.com/maps/api/js?key=AIzaSyBNRABiKOobsfWYTOGSBYu-b4aQXuDeh-A"
        type="text/javascript"></script>
<script src="assets/global/plugins/toastr/toastr.min.js" type="text/javascript"></script>
<!--<script src="../common/assets/global/plugins/quill/quill.min.js" type="text/javascript"></script>-->
<script src="assets/global/plugins/jquery-lazy/jquery.lazy.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/uploader/dist/js/jquery.dm-uploader.min.js"
        type="text/javascript"></script>
<script src="assets/global/plugins/jqueryautocomplete/jquery.autocomplete.pqre.js"
        type="text/javascript"></script>
<!-- END CUSTOM PLUGINS -->
<!-- BEGIN CUSTOM SCRIPTS -->
<script src="assets/global/scripts/global.js" type="text/javascript"></script>
<script src="assets/global/scripts/portlet.js" type="text/javascript"></script>
<script src="assets/global/scripts/modaldata.js" type="text/javascript"></script>
<script src="assets/global/scripts/main.js" type="text/javascript"></script>
<script src="assets/pages/scripts/callbacks.js" type="text/javascript"></script>
<script src="assets/custom/js/custom.js" type="text/javascript"></script>
<!-- END CUSTOM SCRIPTS -->

<script>
    jQuery(document).ready(function () {
        Main.init();
        Portlet.init();

        var array = ["2018-05-14", "2018-05-15", "2018-05-16", "2018-05-17"]

        $('.form_datetime').datetimepicker({
            beforeShowDay: function (date) {
                var string = jQuery.datepicker.formatDate('yy-mm-dd', date);
                return [array.indexOf(string) == -1]
            }
        });
    });

    jQuery(window).load(function () {
        $(".preloader-content-firstload").fadeOut(500, function () {
            $(".preloader-overlay-firstload").fadeOut(1000);
        });

        $('.page-sidebar-menu li').removeClass('open');
        var lia = $("li a[href='" + location.search + "']");

        if (lia.closest('ul').hasClass('page-sidebar-menu')) {
            lia.parent().addClass('open');
        } else if (lia.closest('ul').hasClass('sub-menu')) {
            lia.closest('ul').css({display: 'block'}).parent().addClass('open');
            lia.parent().addClass('active');
        } else {
            $('.page-sidebar-menu li').eq(1).addClass('open');
        }
    });

    jQuery.extend(jQuery.validator.messages, {
        required: "Medan ini wajib diisi.",
        // remote: "Please fix this field.",
        email: "Sila masukkan alamat emel yang sah.",
        // url: "Please enter a valid URL.",
        // date: "Please enter a valid date.",
        // dateISO: "Please enter a valid date (ISO).",
        // number: "Please enter a valid number.",
        digits: "Sila masukkan angka sahaja.",
        // creditcard: "Please enter a valid credit card number.",
        // equalTo: "Please enter the same value again.",
        // accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Sila masukkan tidak lebih dari {0} aksara."),
        minlength: jQuery.validator.format("Sila masukkan sekurang-kurangnya {0} aksara."),
        // rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        // range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Sila masukkan nilai yang lebih besar atau sama dengan {0}."),
        min: jQuery.validator.format("Sila masukkan nilai yang lebih besar atau sama dengan {0}.")
    });
    //var user_id = <?php //echo $_SESSION['user_id']; ?>//;
    //var user_daerah = <?php //echo "'" . $_SESSION['user_daerah'] . "'"; ?>//;
    //var user_negeri = <?php //echo "'" . $_SESSION['user_negeri'] . "'"; ?>//;
    //var sharetoken = <?php //echo "'" . $_SESSION['sharetoken'] . "'"; ?>//;
    //var user_ic_no = <?php //echo "'" . $_SESSION['user_ic_no'] . "'"; ?>//;


</script>
<?php echo $script; ?>
</body>
</html>
