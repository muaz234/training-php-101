<?php
$protocol = "https";

//ABSOLUTE PATH
$path_parts = pathinfo($_SERVER['SCRIPT_FILENAME']);
$absolute_path = str_replace("/backend","",$path_parts['dirname']);

//NATURAL PATH
$server_name = $_SERVER['SERVER_NAME'];
$path_parts = pathinfo($_SERVER['SCRIPT_NAME']);
$directory = str_replace("/backend","",$path_parts['dirname']);
$natural_path = $protocol."://".$server_name.$directory;
$natural_path_img = $protocol."://".$server_name.$directory;

//IMAGES
$images_folder = "/assets/global/img/";
$images_folder_root = "/assets/global/img/";
$currentImageFolderAbsolute = $absolute_path.$images_folder;
$currentImageFolderNatural = $natural_path_img.$images_folder;
$rootImageFolderAbsolute = $absolute_path.$images_folder_root;
$rootImageFolderNatural = $natural_path_img.$images_folder_root;

//WEB SERVICES
$ws_init_url = $natural_path."/common/backend/ws_init.php";
$ws_main_url = $natural_path."/common/backend/ws_main.php";
$_SESSION['ws_init_url'] = $ws_init_url;
?>