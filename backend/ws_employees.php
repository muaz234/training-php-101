<?php
include_once('db_info.php');
if(isset($_GET['method']) && $_GET['method'] == 'senarai_pekerja')
{
    $result = array();
    $sql = "select a.emp_no, a.birth_date, a.first_name, a.last_name, a.gender, DATE_FORMAT(a.hire_date, '%d-%m-%Y') AS hire_date , b.title 
                from employees a inner join titles b on a.emp_no = b.emp_no order by a.emp_no asc limit 100";
    $exec = mysqli_query($db, $sql) or die(mysqli_error($db));
    while($rw = mysqli_fetch_assoc($exec))
    {
        $result[] = [
          'id_pekerja' => $rw['emp_no'],
          'nama_pekerja' => $rw['first_name'] . " " . $rw['last_name'],
          'jantina' => $rw['gender'],
          'tarikh_mula_bekerja' => $rw['hire_date'],
          'jawatan' => $rw['title']
        ];
    }
    echo json_encode(array("data" => $result));
}

if(isset($_GET['method']) && $_GET['method'] == 'get_details_employee')
{
    $employee = array();
    $id = $_GET['id_pekerja'];
    $sql = "select emp_no, first_name, last_name, gender, DATE_FORMAT(hire_date, '%d-%m-%Y') as hire_date from employees where emp_no= '$id' ";
    $exec = mysqli_query($db, $sql) or die(mysqli_error($db));
    $employee = mysqli_fetch_assoc($exec);
    echo json_encode($employee);
}

if(isset($_POST['method']) && $_POST['method'] == 'update_employee')
{
    $status = array();
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $gender = $_POST['gender'];
    $emp_no = $_POST['emp_no'];
    $sql = "UPDATE `employees` SET  first_name = '$first_name', last_name = '$last_name', gender = '$gender' 
            WHERE emp_no = '$emp_no' ";
    $exec = mysqli_query($db, $sql) or die(mysqli_error($db));
//    echo mysqli_affected_rows($db); die();
    if(mysqli_affected_rows($db) > 0)
    {
        $status = ["message" => "success", "statusCode" => 200];
    }
    else
    {
        $status = ["message" => "Unable to update. Error is: ".mysqli_error($db) , 'statusCode' => 400 ];
    }
    echo json_encode($status);
}
